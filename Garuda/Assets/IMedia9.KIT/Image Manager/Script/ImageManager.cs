﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class ImageManager : MonoBehaviour
    {

        public bool isEnabled;

        [Header("Image Path")]
        public string ImagePath;

        [Header("Image Settings")]
        public GameObject ImagePlane;
        public Material ImageMaterial;

        public void LoadImage()
        {
            if (isEnabled)
            {
                if (File.Exists(ImagePath))
                {
                    var bytes = System.IO.File.ReadAllBytes(ImagePath);
                    var tex = new Texture2D(1, 1);
                    tex.LoadImage(bytes);
                    ImageMaterial.mainTexture = tex;

                    MeshRenderer mr = ImagePlane.GetComponent<MeshRenderer>();
                    mr.material = ImageMaterial;
                }
            }
        }

        public void LoadImageFromFile(string aPath)
        {
            if (isEnabled)
            {
                ImagePath = aPath;
                if (File.Exists(ImagePath))
                {
                    var bytes = System.IO.File.ReadAllBytes(ImagePath);
                    var tex = new Texture2D(1, 1);
                    tex.LoadImage(bytes);
                    ImageMaterial.mainTexture = tex;

                    MeshRenderer mr = ImagePlane.GetComponent<MeshRenderer>();
                    mr.material = ImageMaterial;
                }
            }
        }

    }
}
