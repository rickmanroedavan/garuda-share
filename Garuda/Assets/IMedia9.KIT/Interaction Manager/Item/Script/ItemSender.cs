﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk ngirim info Item
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class ItemSender : MonoBehaviour
    {
        public enum COperator { AddValue, SetValue, SubValue }

        public string ItemSenderTag;

        [Header("Calculation Settings")]
        public COperator ItemOperator;
        public float ItemValue;

        [Header("Destroy Settings")]
        public bool DestroyAfterCollision;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}