﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin tampilan UI health - versi ini ditampilin di canvas pada layout screen 
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class UIHealthBarCanvas : MonoBehaviour
    {

        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeVariables;
        public GlobalHealth HealthVariables;
        public GlobalScore ScoreVariables;
        public GlobalIntVar IntVariables;
        public GlobalFloatVar FloatVariables;
        public GlobalStringVar StringVariables;
        public GlobalBoolVar BoolVariables;

        [Header("Display Settings")]
        public Slider DisplaySlider;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            switch (VariableType)
            {
                case GlobalVariable.CVariableType.intVar:
                    DisplaySlider.value = IntVariables.GetCurrentValue();
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    DisplaySlider.value = Mathf.RoundToInt(FloatVariables.GetCurrentValue());
                    break;
                case GlobalVariable.CVariableType.healthVar:
                    DisplaySlider.value = Mathf.RoundToInt(HealthVariables.GetCurrentValue());
                    break;
                case GlobalVariable.CVariableType.scoreVar:
                    DisplaySlider.value = Mathf.RoundToInt(ScoreVariables.GetCurrentValue());
                    break;
                case GlobalVariable.CVariableType.timeVar:
                    DisplaySlider.value = Mathf.RoundToInt(TimeVariables.GetCurrentTimer());
                    break;
            }
            if (DisplaySlider.value <= 0)
            {
                DisplaySlider.transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                DisplaySlider.transform.GetChild(1).gameObject.SetActive(true);
            }

        }
    }
}