﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin tampilan UI health - versi ini ditampilin di lope-lope
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class UIHealthHeartCanvas : MonoBehaviour
    {
        public GlobalHealth HealthVariables;
        [Header("Display Settings")]
        public Toggle[] ToggleHeart;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void LateUpdate()
        {
            int HeartChecker = 1;
            for (int i=0; i < ToggleHeart.Length; i++)
            {
                if (HeartChecker <= Mathf.RoundToInt(HealthVariables.CurrentValue))
                {
                    ToggleHeart[i].isOn = true;
                    HeartChecker++;
                } else
                {
                    ToggleHeart[i].isOn = false;
                }
            }
        }
    }
}
