﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk membuat toggle gameobject/khusus camera
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMGR : MonoBehaviour
{

    public enum CToggleType { ToggleGroup, ToggleEach }
    public CToggleType ToggleType;

    [System.Serializable]
    public class CToggleCamera
    {
        public Camera TargetCamera;
        public KeyCode TriggerKey;
    }

    public CToggleCamera[] ToggleCamera;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < ToggleCamera.Length; i++)
        {
            if (ToggleType == CToggleType.ToggleEach)
            {
                if (Input.GetKeyUp(ToggleCamera[i].TriggerKey))
                {
                    ToggleCamera[i].TargetCamera.gameObject.SetActive(!ToggleCamera[i].TargetCamera.gameObject.activeSelf);
                }
            }
            else if (ToggleType == CToggleType.ToggleGroup)
            {
                if (Input.GetKeyUp(ToggleCamera[i].TriggerKey))
                {
                    for (int j = 0; j < ToggleCamera.Length; j++)
                    {
                        ToggleCamera[j].TargetCamera.gameObject.SetActive(false);
                    }
                    ToggleCamera[i].TargetCamera.gameObject.SetActive(!ToggleCamera[i].TargetCamera.gameObject.activeSelf);
                }
            }
        }
    }
}
