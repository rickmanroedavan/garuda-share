﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin pergerakan camera ala game side-scrolling platfomer
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class CameraSSP : MonoBehaviour
    {
        public bool isEnabled;
        public Camera TargetCamera;
        public GameObject TargetObject;
        public float Delay = 0.1f;
        Vector3 targetDistance;
        Vector3 firstPosition;

        // Use this for initialization
        void Awake()
        {
            firstPosition = TargetCamera.transform.position;
        }

        void Start()
        {
            if (isEnabled)
            {
                Invoke("SetFirstPosition", Delay);
            }
        }

        void SetFirstPosition()
        {
            TargetCamera.transform.position = firstPosition;
            if (TargetObject.activeSelf)
            {
                targetDistance = TargetCamera.transform.position - TargetObject.transform.position;
            }
        }

        // Update is called once per frame
        void LateUpdate()
        {
            if (isEnabled)
            {
               if (TargetObject.activeSelf)
               {
                   TargetCamera.transform.position = TargetObject.transform.position + targetDistance;
               }
            }
        }
    }
}
