﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar fungsi di Unity mulai dari Awake sampai LateUpdate
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/
 
 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedDebug : MonoBehaviour {

    public enum CScriptFor { ThisGameObject, AnotherGameObject }

    public CScriptFor ScriptFor;
    public GameObject AnotherGameObject;

    [Header("Basic Settings")]
    public string GameObjectName;
    public string ConsoleText;

    [Header("Power Ranger Settings")]
    public string AwakeFunction;
    public string StartFunction;
    public string UpdateFunction;
    public string FixedUpdateFunction;
    public string LateUpdateFunction;
    int AwakeCounter = 0;
    int StartCounter = 0;
    int UpdateCounter = 0;
    int FixedUpdateCounter = 0;
    int LateUpdateCounter = 0;

    // Use this for initialization
    void Awake()
    {
        AwakeCounter++;
        AwakeFunction = AwakeCounter.ToString();
    }

    // Use this for initialization
    void Start () {
        StartCounter++;
        StartFunction = StartCounter.ToString();
    }

    void FixedUpdate()
    {
        FixedUpdateCounter++;
        FixedUpdateFunction = FixedUpdateCounter.ToString();
    }

    void LateUpdate()
    {
        LateUpdateCounter++;
        LateUpdateFunction = LateUpdateCounter.ToString();
    }

    // Update is called once per frame
    void Update () {

        UpdateCounter++;
        UpdateFunction = UpdateCounter.ToString();

        if (ScriptFor == CScriptFor.ThisGameObject)
        {
            GameObjectName = this.name;
        } else if (ScriptFor == CScriptFor.AnotherGameObject)
        {
            GameObjectName = AnotherGameObject.name;
        }

        Debug.Log(ConsoleText);
	}
}
