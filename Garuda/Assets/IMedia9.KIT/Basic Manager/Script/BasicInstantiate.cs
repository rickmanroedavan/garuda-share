﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar pergerakan dalam Unity yang terdiri dari Position, Rotation, & Scale.
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicInstantiate : MonoBehaviour {

    public KeyCode TriggerKey;
    public GameObject TargetObject;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(TriggerKey))
        {
            GameObject temp = GameObject.Instantiate(TargetObject, transform.position, transform.rotation);
        }
	}
}
