﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar deteksi Input
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicInput : MonoBehaviour {

    public KeyCode TriggerKey;
    public string DebugCaption;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(TriggerKey)){
            Debug.Log(DebugCaption);
        }
	}
}
