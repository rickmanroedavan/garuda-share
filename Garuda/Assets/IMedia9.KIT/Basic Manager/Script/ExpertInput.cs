﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar input dalam Unity yang terdiri dari klik, mencet dan neken.
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpertInput : MonoBehaviour {

    public enum CScriptFor { ThisGameObject, AnotherGameObject }

    public CScriptFor ScriptFor;
    public GameObject AnotherGameObject;

    [Header("Keyboard Settings")]
    public bool isEnabled;
    public KeyCode LeftKey;
    public KeyCode RightKey;
    public KeyCode UpKey;
    public KeyCode DownKey;
    public KeyCode RotateLeft;
    public KeyCode RotateRight;
    public float Speed;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isEnabled)
        {
            if (ScriptFor == CScriptFor.ThisGameObject)
            {
                if (Input.GetKey(UpKey))
                {
                    this.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(DownKey))
                {
                    this.transform.Translate(-Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RightKey))
                {
                    this.transform.Translate(Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(LeftKey))
                {
                    this.transform.Translate(-Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RotateLeft))
                {
                    this.transform.Rotate(0, -1, 0); 
                }
                if (Input.GetKey(RotateRight))
                {
                    this.transform.Rotate(0, 1, 0);
                }
            } else
            if (ScriptFor == CScriptFor.AnotherGameObject)
            {
                if (Input.GetKey(UpKey))
                {
                    AnotherGameObject.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(DownKey))
                {
                    AnotherGameObject.transform.Translate(-Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RightKey))
                {
                    AnotherGameObject.transform.Translate(Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(LeftKey))
                {
                    AnotherGameObject.transform.Translate(-Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RotateLeft))
                {
                    AnotherGameObject.transform.Rotate(0, -1, 0);
                }
                if (Input.GetKey(RotateRight))
                {
                    AnotherGameObject.transform.Rotate(0, 1, 0);
                }
            }
        }
    }
}
