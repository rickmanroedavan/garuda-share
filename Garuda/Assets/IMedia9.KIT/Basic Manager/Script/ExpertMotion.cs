﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar pergerakan dalam Unity yang terdiri dari Position, Rotation, & Scale.
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace IMedia9
{
    public class ExpertMotion : MonoBehaviour
    {
        public enum CScriptFor { ThisGameObject, AnotherGameObject }
        public enum CVectorType { ExactValue, IncrementValue }

        public CScriptFor ScriptFor;
        public GameObject AnotherGameObject;

        [Header("Position Settings")]
        public bool usingPosition;
        public CVectorType PositionType;
        public Vector3 NewPosition;

        [Header("Rotation Settings")]
        public bool usingRotation;
        public CVectorType RotationType;
        public Vector3 NewRotation;

        [Header("Scale Settings")]
        public bool usingScale;
        public CVectorType ScaleType;
        public Vector3 NewScale;

        [Header("Translate Settings")]
        public bool usingTranslate;
        public CVectorType TranslateType;
        public Vector3 NewTranslate;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (usingPosition)
            {
                if (PositionType == CVectorType.ExactValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.position = NewPosition;
                    } else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.position = NewPosition;
                    }
                } else
                if (PositionType == CVectorType.IncrementValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.position += NewPosition;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.position += NewPosition;
                    }
                }
            }
            if (usingRotation)
            {
                if (RotationType == CVectorType.ExactValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.localEulerAngles = NewRotation;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.localEulerAngles = NewRotation;
                    }
                }
                else
                if (RotationType == CVectorType.IncrementValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.localEulerAngles += NewRotation;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.localEulerAngles += NewRotation;
                    }
                }
            }
            if (usingScale)
            {
                if (ScaleType == CVectorType.ExactValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.localScale = NewScale;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.localScale = NewScale;
                    }
                }
                else
                if (ScaleType == CVectorType.IncrementValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.localScale += NewScale;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.localScale += NewScale;
                    }
                }
            }
            if (usingTranslate)
            {
                if (TranslateType == CVectorType.ExactValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.transform.position = NewTranslate;
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.position = NewTranslate;
                    }
                }
                else
                if (TranslateType == CVectorType.IncrementValue)
                {
                    if (ScriptFor == CScriptFor.ThisGameObject)
                    {
                        this.transform.Translate(NewTranslate);
                    }
                    else
                    if (ScriptFor == CScriptFor.AnotherGameObject)
                    {
                        AnotherGameObject.transform.Translate(NewTranslate);
                    }
                }
            }

        }
    }
}
