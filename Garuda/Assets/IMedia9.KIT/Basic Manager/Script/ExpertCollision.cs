﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk menunjukkan dasar-dasar collision di Unity yang meliputi collision & trigger
 *          Script ini ditujukan bagi anda yang males ngoding, skill copas masih cupu dan cuma bisa ngetik
 *          dengan jempol. Oh syaaap! 
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpertCollision : MonoBehaviour
{

    public enum CScriptFor { ThisGameObject }
    public enum CCollisionType { IsCollision, IsTrigger }

    public CScriptFor ScriptFor;

    [Header("Keyboard Settings")]
    public bool isEnabled;
    public KeyCode LeftKey;
    public KeyCode RightKey;
    public KeyCode UpKey;
    public KeyCode DownKey;
    public KeyCode RotateLeft;
    public KeyCode RotateRight;
    public float Speed;

    [Header("Collision Settings")]
    public CCollisionType CollisionType;

    [Header("Debug Value")]
    public string DebugText;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isEnabled)
        {
            if (ScriptFor == CScriptFor.ThisGameObject)
            {
                if (Input.GetKey(UpKey))
                {
                    this.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(DownKey))
                {
                    this.transform.Translate(-Vector3.forward * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RightKey))
                {
                    this.transform.Translate(Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(LeftKey))
                {
                    this.transform.Translate(-Vector3.right * Speed * Time.deltaTime);
                }
                if (Input.GetKey(RotateLeft))
                {
                    this.transform.Rotate(0, -1, 0);
                }
                if (Input.GetKey(RotateRight))
                {
                    this.transform.Rotate(0, 1, 0);
                }
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (CollisionType == CCollisionType.IsCollision)
        {
            DebugText = collision.gameObject.tag + ":" + collision.gameObject.transform.position;
            Debug.Log(DebugText);
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (CollisionType == CCollisionType.IsTrigger)
        {
            DebugText = collider.tag + ":" + collider.gameObject.transform.position;
            Debug.Log(DebugText);
        }
    }

}
