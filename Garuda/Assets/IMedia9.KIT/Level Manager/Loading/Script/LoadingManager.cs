﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace IMedia9
{
    public class LoadingManager : MonoBehaviour
    {

        public static string NextSceneIndex = "NextSceneIndex";
        int scene;
        public Slider LoadingBar;
        Slider loadbar;

        // Use this for initialization
        void Start()
        {
            scene = PlayerPrefs.GetInt(LoadingManager.NextSceneIndex);
            if (scene != 0)
            {
                StartCoroutine(LoadNewScene());
            }
        }

        // Update is called once per frame
        void Update()
        {

        }

        IEnumerator LoadNewScene()
        {

            yield return new WaitForSeconds(1);

            AsyncOperation async = SceneManager.LoadSceneAsync(scene);

            while (!async.isDone)
            {
                Debug.Log(async.progress);
                LoadingBar.value = ((async.progress / 0.9f) * 100);
                yield return null;
            }

        }

    }
}