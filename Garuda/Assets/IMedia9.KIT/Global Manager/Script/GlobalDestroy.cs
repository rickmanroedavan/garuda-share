﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk mendapatkan score karena hati yang hancur
 **************************************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalDestroy : MonoBehaviour
    {

        public float CurrentValue;

        [Header("Global Score")]
        public GlobalScore ScoreVariables;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public float GetCurrentValue()
        {
            return CurrentValue;
        }

        public void AddToCurrentValue(float aValue)
        {
            CurrentValue += aValue;
        }

        public void SubToCurrentValue(float aValue)
        {
            CurrentValue -= aValue;
        }

        public bool IsShutdown()
        {
            return CurrentValue <= 0;
        }

        public void DestroyAddScore()
        {
            ScoreVariables.AddToCurrentValue(CurrentValue);
        }

        void OnDestroy()
        {
            DestroyAddScore();
        }
    }

}