﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk membuat global transfer antara global variabel
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class GlobalTransfer : MonoBehaviour
    {
        [Space(10)]
        public bool isEnabled;

        [Header("Global Variable")]
        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeSender;
        public GlobalTime TimeReceiver;
        public GlobalHealth HealthSender;
        public GlobalHealth HealthReceiver;
        public GlobalScore ScoreSender;
        public GlobalScore ScoreReceiver;
        public GlobalIntVar IntSender;
        public GlobalIntVar IntReceiver;
        public GlobalFloatVar FloatSender;
        public GlobalFloatVar FloatReceiver;
        public GlobalStringVar StringSender;
        public GlobalStringVar StringReceiver;
        public GlobalBoolVar BoolSender;
        public GlobalBoolVar BoolReceiver;

        [Header("Increment Settings")]
        public float Increment;

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled)
            {
                if (VariableType == GlobalVariable.CVariableType.scoreVar)
                {
                    if (ScoreReceiver.CurrentValue < ScoreSender.CurrentValue)
                    {
                        ScoreReceiver.AddToCurrentValue(Increment + 1);
                    }
                    if (ScoreReceiver.CurrentValue >= ScoreSender.CurrentValue)
                    {
                        ScoreReceiver.CurrentValue = ScoreSender.CurrentValue;
                    }
                }
            }
        }

    }
}
