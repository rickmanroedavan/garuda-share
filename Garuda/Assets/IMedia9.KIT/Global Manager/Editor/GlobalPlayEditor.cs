﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script editor untuk global play
 **************************************************************************************************************/

using UnityEditor;
using UnityEngine;

namespace IMedia9
{
    [CustomEditor(typeof(GlobalPlay)), CanEditMultipleObjects]
    public class GlobalPlayEditor : Editor
    {

        public SerializedProperty
            GameType_prop,
            GameStatus_prop,
            TargetObject_prop,
            TargetAudio_prop
            ;

        void OnEnable()
        {
            // Setup the SerializedProperties
            GameType_prop = serializedObject.FindProperty("GameType");
            GameStatus_prop = serializedObject.FindProperty("GameStatus");
            TargetObject_prop = serializedObject.FindProperty("TargetObject");
            TargetAudio_prop = serializedObject.FindProperty("TargetAudio");

        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(GameType_prop);

            GlobalPlay.CGameType st = (GlobalPlay.CGameType) GameType_prop.enumValueIndex;

            switch (st)
            {
                case GlobalPlay.CGameType.TimeScaleMode:
                    EditorGUILayout.PropertyField(GameStatus_prop, true);
                    break;
                case GlobalPlay.CGameType.GameObjectMode:
                    EditorGUILayout.PropertyField(GameStatus_prop, true);
                    EditorGUILayout.PropertyField(TargetObject_prop, true);
                    EditorGUILayout.PropertyField(TargetAudio_prop, true);
                    break;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
