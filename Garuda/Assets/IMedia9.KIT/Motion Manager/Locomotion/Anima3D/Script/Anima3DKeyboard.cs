﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{

    public class Anima3DKeyboard : MonoBehaviour
    {

        public enum CCompareType { Greater, Equal, Less }
        public enum CParameterType { Int, Float, Bool, Trigger }

        public bool isEnabled;
        public Animator TargetAnimator;

        [System.Serializable]
        public class CMovingState3D
        {
            public string StateNow;
            public string StateNext;
            public CParameterType ParameterType;
            public string ParameterName;
            public string PositiveValue;
            public string NegativeValue;
            public KeyCode[] TriggerKey;
            public bool ForceAnimation;
            [Header("Sound Settings")]
            public bool usingSound;
            public AudioSource animaAudioSource;
            public AudioClip animaAudioClip;
        }

        [System.Serializable]
        public class CActionState3D
        {
            public string StateNow;
            public string StateNext;
            public CParameterType ParameterType;
            public string ParameterName;
            public string PositiveValue;
            public string NegativeValue;
            public KeyCode[] TriggerKey;
            public bool ForceAnimation;
            [Header("Sound Settings")]
            public bool usingSound;
            public AudioSource animaAudioSource;
            public AudioClip animaAudioClip;
        }

        [System.Serializable]
        public class CShutdownState3D
        {
            public string StateNow;
            public string StateNext;
            public CParameterType ParameterType;
            public string ParameterName;
            public string PositiveValue;
            public string NegativeValue;
            public KeyCode[] ShutdownTestkey;

            [Header("Disable Input Settings")]
            public bool DisabledInputWhenDeath;
            public Mechanim3DKeyboard InputKeyboard;

            [Header("Sound Settings")]
            public bool usingSound;
            public AudioSource animaAudioSource;
            public AudioClip animaAudioClip;
        }

        [Header("Moving Settings")]
        public bool usingMovingState;
        public CMovingState3D[] MovingState3D;

        [Header("Action Settings")]
        public bool usingActionState;
        public CActionState3D[] ActionState3D;

        [Header("Shutdown Settings")]
        public bool usingShutdownState;
        public CShutdownState3D ShutdownState3D;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (isEnabled && usingMovingState)
            {
                for (int i = 0; i < MovingState3D.Length; i++)
                {
                    for (int j = 0; j < MovingState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKey(MovingState3D[i].TriggerKey[j]))
                        {
                            if (MovingState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(MovingState3D[i].PositiveValue);
                                TargetAnimator.SetFloat(MovingState3D[i].ParameterName, dummyvalue);
                                if (MovingState3D[i].ForceAnimation)
                                {
                                    if (!TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(MovingState3D[i].StateNext))
                                    {
                                        TargetAnimator.Play(MovingState3D[i].StateNext);
                                    }
                                }
                                ExecuteMovingSound(i);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(MovingState3D[i].PositiveValue);
                                TargetAnimator.SetInteger(MovingState3D[i].ParameterName, dummyvalue);
                                if (MovingState3D[i].ForceAnimation)
                                {
                                    if (!TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(MovingState3D[i].StateNext))
                                    {
                                        TargetAnimator.Play(MovingState3D[i].StateNext);
                                    }
                                }
                                ExecuteMovingSound(i);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(MovingState3D[i].PositiveValue);
                                TargetAnimator.SetBool(MovingState3D[i].ParameterName, dummyvalue);
                                if (MovingState3D[i].ForceAnimation)
                                {
                                    if (!TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(MovingState3D[i].StateNext))
                                    {
                                        TargetAnimator.Play(MovingState3D[i].StateNext);
                                    }
                                }
                                ExecuteMovingSound(i);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                TargetAnimator.SetTrigger(MovingState3D[i].ParameterName);
                                if (MovingState3D[i].ForceAnimation)
                                {
                                    if (!TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(MovingState3D[i].StateNext))
                                    {
                                        TargetAnimator.Play(MovingState3D[i].StateNext);
                                    }
                                }
                                ExecuteMovingSound(i);
                            }
                        }
                    }
                }
            }

            if (isEnabled && usingActionState)
            {
                for (int i = 0; i < ActionState3D.Length; i++)
                {
                    for (int j = 0; j < ActionState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKey(ActionState3D[i].TriggerKey[j]))
                        {
                            if (ActionState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ActionState3D[i].PositiveValue);
                                TargetAnimator.SetFloat(ActionState3D[i].ParameterName, dummyvalue);
                                ExecuteActionSound(i);
                            }
                            if (ActionState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(ActionState3D[i].PositiveValue);
                                TargetAnimator.SetInteger(ActionState3D[i].ParameterName, dummyvalue);
                                ExecuteActionSound(i);
                            }
                            if (ActionState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(ActionState3D[i].PositiveValue);
                                TargetAnimator.SetBool(ActionState3D[i].ParameterName, dummyvalue);
                                ExecuteActionSound(i);
                            }
                            if (ActionState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                TargetAnimator.SetTrigger(ActionState3D[i].ParameterName);
                                ExecuteActionSound(i);
                            }
                        }
                    }
                }
            }

            if (isEnabled)
            {
                for (int i = 0; i < ShutdownState3D.ShutdownTestkey.Length; i++)
                {
                    if (Input.GetKey(ShutdownState3D.ShutdownTestkey[i]))
                    {
                        Shutdown(true);
                    }
                }
            }
        }

        void LateUpdate()
        {
            if (isEnabled && usingMovingState)
            {
                for (int i = 0; i < MovingState3D.Length; i++)
                {
                    for (int j = 0; j < MovingState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKeyUp(MovingState3D[i].TriggerKey[j]))
                        {
                            if (MovingState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(MovingState3D[i].NegativeValue);
                                TargetAnimator.SetFloat(MovingState3D[i].ParameterName, dummyvalue);
                                ExecuteMovingSound(i, false);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(MovingState3D[i].NegativeValue);
                                TargetAnimator.SetInteger(MovingState3D[i].ParameterName, dummyvalue);
                                ExecuteMovingSound(i, false);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(MovingState3D[i].NegativeValue);
                                TargetAnimator.SetBool(MovingState3D[i].ParameterName, dummyvalue);
                                ExecuteMovingSound(i, false);
                            }
                            if (MovingState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                TargetAnimator.SetTrigger(MovingState3D[i].ParameterName);
                                ExecuteMovingSound(i, false);
                            }

                        }
                    }

                }
            }

            if (isEnabled && usingActionState)
            {
                for (int i = 0; i < ActionState3D.Length; i++)
                {
                    for (int j = 0; j < ActionState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKeyUp(ActionState3D[i].TriggerKey[j]))
                        {
                            if (ActionState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ActionState3D[i].NegativeValue);
                                TargetAnimator.SetFloat(ActionState3D[i].ParameterName, dummyvalue);
                                ExecuteActionSound(i, false);
                            }
                        }
                    }
                }
            }
        }

        void Shutdown(bool aValue)
        {
            if (usingShutdownState)
            {
                if (ShutdownState3D.ParameterType == CParameterType.Float)
                {
                    float dummyvalue = float.Parse(ShutdownState3D.PositiveValue);
                    TargetAnimator.SetFloat(ShutdownState3D.ParameterName, dummyvalue);
                    ExecuteShutdownSound();
                }
                if (ShutdownState3D.ParameterType == CParameterType.Int)
                {
                    int dummyvalue = int.Parse(ShutdownState3D.PositiveValue);
                    TargetAnimator.SetInteger(ShutdownState3D.ParameterName, dummyvalue);
                    ExecuteShutdownSound();
                }
                if (ShutdownState3D.ParameterType == CParameterType.Bool)
                {
                    bool dummyvalue = bool.Parse(ShutdownState3D.PositiveValue);
                    TargetAnimator.SetBool(ShutdownState3D.ParameterName, dummyvalue);
                    ExecuteShutdownSound();
                }
                if (ShutdownState3D.ParameterType == CParameterType.Trigger)
                {
                    TargetAnimator.SetTrigger(ShutdownState3D.ParameterName);
                    ExecuteShutdownSound();
                }

                if (ShutdownState3D.DisabledInputWhenDeath)
                {
                    if (ShutdownState3D.InputKeyboard != null)
                    {
                        ShutdownState3D.InputKeyboard.enabled = false;
                    }
                }
            }
        }

        void ExecuteShutdownSound(bool plays = true)
        {
            if (ShutdownState3D.usingSound)
            {
                if (!ShutdownState3D.animaAudioSource.isPlaying && plays)
                {
                    ShutdownState3D.animaAudioSource.clip = ShutdownState3D.animaAudioClip;
                    ShutdownState3D.animaAudioSource.Play();
                }
                else if (!plays)
                {
                    ShutdownState3D.animaAudioSource.clip = ShutdownState3D.animaAudioClip;
                    ShutdownState3D.animaAudioSource.Stop();
                }
            }
        }

        void ExecuteActionSound(int index, bool plays = true)
        {
            if (ActionState3D[index].usingSound)
            {
                if (!ActionState3D[index].animaAudioSource.isPlaying && plays)
                {
                    ActionState3D[index].animaAudioSource.clip = ActionState3D[index].animaAudioClip;
                    ActionState3D[index].animaAudioSource.Play();
                }
                else if (!plays)
                {
                    ActionState3D[index].animaAudioSource.clip = ActionState3D[index].animaAudioClip;
                    ActionState3D[index].animaAudioSource.Stop();
                }
            }
        }


        void ExecuteMovingSound(int index, bool plays = true)
        {
            if (MovingState3D[index].usingSound)
            {
                if (!MovingState3D[index].animaAudioSource.isPlaying && plays)
                {
                    MovingState3D[index].animaAudioSource.clip = MovingState3D[index].animaAudioClip;
                    MovingState3D[index].animaAudioSource.Play();
                }
                else if (!plays)
                {
                    MovingState3D[index].animaAudioSource.clip = MovingState3D[index].animaAudioClip;
                    MovingState3D[index].animaAudioSource.Stop();
                }
            }
        }

    }

}