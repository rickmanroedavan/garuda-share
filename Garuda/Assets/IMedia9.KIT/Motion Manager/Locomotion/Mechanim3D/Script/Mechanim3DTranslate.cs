﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk perubahan posisi objek non-karakter menggunakan Translate
 **************************************************************************************************************/
using UnityEngine;
using System.Collections;

namespace IMedia9
{

    public class Mechanim3DTranslate : MonoBehaviour
    {
        public enum CMovementType { SingleDirection, AllDirection, OctaDirection }

        public CMovementType MovementType;

        [Header("Primary Keys")]
        public KeyCode UpKey = KeyCode.UpArrow;
        public KeyCode LeftKey = KeyCode.LeftArrow;
        public KeyCode DownKey = KeyCode.DownArrow;
        public KeyCode RightKey = KeyCode.RightArrow;
        public KeyCode JumpKey;
        public KeyCode ShiftKey;
        [Space(10)]

        [Header("Alternative Keys")]
        public KeyCode AltUpKey = KeyCode.W;
        public KeyCode AltLeftKey = KeyCode.A;
        public KeyCode AltDownKey = KeyCode.S;
        public KeyCode AltRightKey = KeyCode.D;
        public KeyCode AltJumpKey;
        public KeyCode AltShiftKey;
        [Space(10)]


        [Header("Movement Settings")]
        public float MoveSpeed = 200;
        public float RotateSpeed = 5;
        public float jumpSpeed = 8.0F;
        public float gravity = 20.0F;
        private Vector3 moveDirection = Vector3.zero;

        GameObject TargetController;

        void Start()
        {
            TargetController = this.gameObject;
            gravity = 0;
        }

        void Update()
        {
            if (MovementType == CMovementType.SingleDirection)
            {

                moveDirection = new Vector3(GetAxisHorizontal(), 0, GetAxisVertical());
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= MoveSpeed;
                if (Input.GetKey(JumpKey))
                    moveDirection.y = jumpSpeed;
                    
                moveDirection.y -= gravity * Time.deltaTime;
                TargetController.transform.position += moveDirection * Time.deltaTime;
            }
            else if (MovementType == CMovementType.AllDirection)
            {
                    moveDirection = new Vector3(GetAxisHorizontal(), 0, GetAxisVertical());
                    moveDirection = transform.TransformDirection(moveDirection);
                    moveDirection *= MoveSpeed;

                    if (Input.GetKey(LeftKey) || Input.GetKey(AltLeftKey))
                    {
                        transform.Rotate(0, -RotateSpeed, 0);
                    }
                    if (Input.GetKey(RightKey) || Input.GetKey(AltRightKey))
                    {
                        transform.Rotate(0, RotateSpeed, 0);
                    }

                    if (Input.GetKey(JumpKey) || Input.GetKey(AltJumpKey))
                        moveDirection.y = jumpSpeed;

                moveDirection.y -= gravity * Time.deltaTime;
                TargetController.transform.position += moveDirection * Time.deltaTime;
            }
            else if (MovementType == CMovementType.OctaDirection)
            {
                    if (Input.GetKey(UpKey) && Input.GetKey(RightKey) || Input.GetKey(AltUpKey) && Input.GetKey(AltRightKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 45, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(UpKey) && Input.GetKey(LeftKey) || Input.GetKey(AltUpKey) && Input.GetKey(AltLeftKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 315, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(DownKey) && Input.GetKey(RightKey) || Input.GetKey(AltDownKey) && Input.GetKey(AltRightKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 135, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(DownKey) && Input.GetKey(LeftKey) || Input.GetKey(AltDownKey) && Input.GetKey(AltLeftKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 225, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(UpKey) || Input.GetKey(AltUpKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 0, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(DownKey) || Input.GetKey(AltDownKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 180, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(LeftKey) || Input.GetKey(AltLeftKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 270, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }
                    else
                    if (Input.GetKey(RightKey) || Input.GetKey(AltRightKey))
                    {
                        this.transform.localEulerAngles = new Vector3(0, 90, 0);
                        moveDirection = transform.forward * MoveSpeed * Time.deltaTime;
                        TargetController.transform.position += moveDirection;
                    }


                    if (Input.GetKeyUp(UpKey) || Input.GetKeyUp(DownKey) || Input.GetKeyUp(LeftKey) || Input.GetKeyUp(RightKey) ||
                        Input.GetKeyUp(AltUpKey) || Input.GetKeyUp(AltDownKey) || Input.GetKeyUp(AltLeftKey) || Input.GetKeyUp(AltRightKey))
                    {
                        moveDirection = Vector3.zero;
                    }
                    if (Input.GetKey(JumpKey) || Input.GetKey(AltJumpKey))
                        moveDirection.y = jumpSpeed;

                moveDirection.y -= gravity * Time.deltaTime;
                TargetController.transform.position += moveDirection;
            }
        }

        float GetAxisHorizontal()
        {
            float result = 0;
            if (Input.GetKey(LeftKey) || Input.GetKey(AltLeftKey))
            {
                result = -1;
            }
            if (Input.GetKey(RightKey) || Input.GetKey(AltRightKey))
            {
                result = 1;
            }
            return result;
        }

        float GetAxisVertical()
        {
            float result = 0;
            if (Input.GetKey(UpKey) || Input.GetKey(AltUpKey))
            {
                result = 1;
            }
            if (Input.GetKey(DownKey) || Input.GetKey(AltDownKey))
            {
                result = -1;
            }
            return result;
        }

    }
}