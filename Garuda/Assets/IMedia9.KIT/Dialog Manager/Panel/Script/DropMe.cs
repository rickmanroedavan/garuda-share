using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace IMedia9
{

    public class DropMe : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
    {
        public Image receivingImage;

        [Header("Drop Settings")]
        public Image containerImage;
        public Color highlightColor = Color.yellow;
        private Color normalColor;

        [Header("Destroy Settings")]
        public bool DestroySender;
        public float DestroyDelay = 0.3f;
        GameObject lastObject;

        public void OnEnable()
        {
            if (containerImage != null)
                normalColor = containerImage.color;
        }

        public void OnDrop(PointerEventData data)
        {
            containerImage.color = normalColor;

            if (receivingImage == null)
                return;

            Sprite dropSprite = GetDropSprite(data);
            if (dropSprite != null)
                receivingImage.overrideSprite = dropSprite;

            Debug.Log("Testing");
        }

        public void OnPointerEnter(PointerEventData data)
        {
            if (containerImage == null)
                return;

            Sprite dropSprite = GetDropSprite(data);
            if (dropSprite != null)
                containerImage.color = highlightColor;

            if (DestroySender)
            {
                var originalObj = data.pointerDrag;
                if (originalObj != null)
                {
                    lastObject = originalObj;
                }
            }

            Destroy(lastObject, DestroyDelay);
        }

        public void OnPointerExit(PointerEventData data)
        {
            if (containerImage == null)
                return;

            containerImage.color = normalColor;
        }

        private Sprite GetDropSprite(PointerEventData data)
        {
            var originalObj = data.pointerDrag;
            if (originalObj == null)
                return null;

            var dragMe = originalObj.GetComponent<DragMe>();
            if (dragMe == null)
                return null;

            var srcImage = originalObj.GetComponent<Image>();
            if (srcImage == null)
                return null;

            return srcImage.sprite;
        }
    }
}
