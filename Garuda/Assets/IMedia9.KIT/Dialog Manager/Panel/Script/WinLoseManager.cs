﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin mekanisme win/lose
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    public class WinLoseManager : MonoBehaviour
    {
        public enum CConditionType { WinCondition, LoseCondition };
        [Space(10)]
        public bool isEnabled;

        [Header("Condition Settings")]
        public CConditionType ConditionType;

        [Space(10)]
        public GlobalVariable.CVariableType VariableType;
        public GlobalTime TimeVariables;
        public int TimeValue;
        public GlobalHealth HealthVariables;
        public int HealthValue;
        public GlobalScore ScoreVariables;
        public int ScoreValue;
        public GlobalIntVar IntVariables;
        public int IntValue;
        public GlobalFloatVar FloatVariables;
        public float FloatValue;
        public GlobalStringVar StringVariables;
        public string StringValue;
        public GlobalBoolVar BoolVariables;
        public bool BoolValue;

        [Header("Canvas Dialog")]
        public bool usingCanvas;
        public Canvas CanvasDialog;

        [Header("Panel Dialog")]
        public bool usingPanel;
        public Image PanelDialog;

        [Header("Other Settings")]
        public int DelayExecute;
        public GameObject[] ShutdownGameObject;

        // Use this for initialization
        void Awake()
        {
            if (usingCanvas)
            {
                CanvasDialog.gameObject.SetActive(false);
            }
            if (usingPanel)
            {
                PanelDialog.gameObject.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (ConditionType == CConditionType.WinCondition) {
                if (VariableType == GlobalVariable.CVariableType.timeVar)
                {
                    if (TimeVariables.CurrentTime <= TimeValue)
                    {
                        ExecuteWin();
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.healthVar)
                {
                    if (HealthVariables.CurrentValue <= HealthValue)
                    {
                        ExecuteWin();
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.scoreVar)
                {
                    if (ScoreVariables.CurrentValue <= ScoreValue)
                    {
                        ExecuteWin();
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.floatVar)
                {
                    if (FloatVariables.CurrentValue <= FloatValue) {
                        ExecuteWin();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.boolVar)
                {
                    if (BoolVariables.CurrentValue == BoolValue)
                    {
                        ExecuteWin();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.intVar)
                {
                    if (IntVariables.CurrentValue <= IntValue)
                    {
                        ExecuteWin();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.stringVar)
                {
                    if (StringVariables.CurrentValue == StringValue)
                    {
                        ExecuteWin();
                    }
                }
            }
            if (ConditionType == CConditionType.LoseCondition)
            {
                if (VariableType == GlobalVariable.CVariableType.timeVar)
                {
                    if (TimeVariables.CurrentTime <= TimeValue)
                    {
                        ExecuteLose();
                    }
                }

                if (VariableType == GlobalVariable.CVariableType.healthVar)
                {
                    if (HealthVariables.CurrentValue <= HealthValue)
                    {
                        ExecuteLose();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.floatVar)
                {
                    if (FloatVariables.CurrentValue <= FloatValue)
                    {
                        ExecuteLose();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.boolVar)
                {
                    if (BoolVariables.CurrentValue == BoolValue)
                    {
                        ExecuteLose();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.intVar)
                {
                    if (IntVariables.CurrentValue <= IntValue)
                    {
                        ExecuteLose();
                    }
                }
                if (VariableType == GlobalVariable.CVariableType.stringVar)
                {
                    if (StringVariables.CurrentValue == StringValue)
                    {
                        ExecuteLose();
                    }
                }
            }
        }

        void ExecuteWin() {
            Invoke("TriggerWin", DelayExecute);
        }

        void ExecuteLose()
        {
            Invoke("TriggerLose", DelayExecute);
        }

        void TriggerWin()
        {
            if (usingCanvas)
            {
                CanvasDialog.gameObject.SetActive(true);
            }
            if (usingPanel)
            {
                PanelDialog.gameObject.SetActive(true);
            }
            for (int i = 0; i < ShutdownGameObject.Length; i++)
            {
                ShutdownGameObject[i].SendMessage("Shutdown");
            }
        }

        void TriggerLose()
        {
            if (usingCanvas)
            {
                CanvasDialog.gameObject.SetActive(true);
            }
            if (usingPanel)
            {
                PanelDialog.gameObject.SetActive(true);
            }
            for (int i = 0; i < ShutdownGameObject.Length; i++)
            {
                ShutdownGameObject[i].SendMessage("Shutdown");
            }
        }

        void Pause()
        {
            Time.timeScale = 0;
        }
    }
}
