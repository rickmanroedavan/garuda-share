﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script untuk bikin mekanismea pause dengan membuat time scale = 0
 **************************************************************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class PauseManager : MonoBehaviour
    {

        public bool isEnabled;

        public enum CGameType { TimeScaleMode, GameObjectMode }
        public enum CGameStatus { IsPlay, IsPause }

        [Header("Pause Mode")]
        public CGameType GameType;
        public CGameStatus GameStatus;

        [Header("Panel Settings")]
        public KeyCode TriggerPanelKey;
        public Image TargetPanel;

        [HideInInspector]
        public bool PauseGamePlay;

        [Header("Play/Pause Settings")]
        public GameObject[] TargetObject;
        public AudioSource[] TargetAudio;

        bool statusPause;

        // Use this for initialization
        void Awake()
        {
            statusPause = false;
            PauseGamePlay = true;
            TargetPanel.gameObject.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(TriggerPanelKey))
            {
                if (GameStatus == CGameStatus.IsPlay)
                {
                    GameStatus = CGameStatus.IsPause;
                }
                else if (GameStatus == CGameStatus.IsPause)
                {
                    GameStatus = CGameStatus.IsPlay;
                }
            }

            if (Input.GetKeyUp(TriggerPanelKey))
            {
                if (GameStatus == CGameStatus.IsPause)
                {
                    statusPause = true;
                    SetGamePause();
                }
                else if (GameStatus == CGameStatus.IsPlay)
                {
                    statusPause = false;
                    SetGamePlay();
                }
            }
        }

        public void SetGamePause()
        {
            if (GameType == CGameType.TimeScaleMode)
            {
                TargetPanel.transform.gameObject.SetActive(true);
                if (PauseGamePlay)
                {
                    Time.timeScale = 0;
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SetActive(false);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].Pause();
                    }
                }
            }
            else if (GameType == CGameType.GameObjectMode)
            {
                TargetPanel.transform.gameObject.SetActive(true);
                if (PauseGamePlay)
                {
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SetActive(false);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].Pause();
                    }
                }
            }
        }

        public void SetGamePlay()
        {
            if (GameType == CGameType.TimeScaleMode)
            {
                TargetPanel.transform.gameObject.SetActive(false);
                if (PauseGamePlay)
                {
                    Time.timeScale = 1;
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SetActive(false);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].UnPause();
                    }
                }
            }
            else if (GameType == CGameType.GameObjectMode)
            {
                TargetPanel.transform.gameObject.SetActive(false);
                if (PauseGamePlay)
                {
                    for (int i = 0; i < TargetObject.Length; i++)
                    {
                        TargetObject[i].SetActive(false);
                    }
                    for (int i = 0; i < TargetAudio.Length; i++)
                    {
                        TargetAudio[i].UnPause();
                    }
                }

            }
        }
    }

}
