﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script editor win/lose manager
 **************************************************************************************************************/

using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{
    [CustomEditor(typeof(WinLoseManager)), CanEditMultipleObjects]
    public class WinLoseManagerEditor : Editor
    {

        public SerializedProperty
            enabled_prop,
            enum_Condition,
            enum_Status,
            timeVariables_prop,
            timeValue_prop,
            healthVariables_prop,
            healthValue_prop,
            scoreVariables_prop,
            scoreValue_prop,
            intVariables_prop,
            intValue_prop,
            floatVariables_prop,
            floatValue_prop,
            boolVariables_prop,
            boolValue_prop,
            stringVariables_prop,
            stringValue_prop,
            delayExecute_prop,
            usingcanvas_prop,
            canvasdialog_prop,
            usingpanel_prop,
            paneldialog_prop,
            shutdownobject_prop
            ;

        void OnEnable()
        {
            // Setup the SerializedProperties
            enabled_prop = serializedObject.FindProperty("isEnabled");
            enum_Condition = serializedObject.FindProperty("ConditionType");
            enum_Status = serializedObject.FindProperty("VariableType");
            timeVariables_prop = serializedObject.FindProperty("TimeVariables");
            timeValue_prop = serializedObject.FindProperty("TimeValue");
            healthVariables_prop = serializedObject.FindProperty("HealthVariables");
            healthValue_prop = serializedObject.FindProperty("HealthValue");
            scoreVariables_prop = serializedObject.FindProperty("ScoreVariables");
            scoreValue_prop = serializedObject.FindProperty("ScoreValue");
            intVariables_prop = serializedObject.FindProperty("IntVariables");
            intValue_prop = serializedObject.FindProperty("IntValue");
            floatVariables_prop = serializedObject.FindProperty("FloatVariables");
            floatValue_prop = serializedObject.FindProperty("FloatValue");
            boolVariables_prop = serializedObject.FindProperty("BoolVariables");
            boolValue_prop = serializedObject.FindProperty("BoolValue");
            stringVariables_prop = serializedObject.FindProperty("StringVariables");
            stringValue_prop = serializedObject.FindProperty("StringValue");
            delayExecute_prop = serializedObject.FindProperty("DelayExecute");
            usingcanvas_prop = serializedObject.FindProperty("usingCanvas");
            canvasdialog_prop = serializedObject.FindProperty("CanvasDialog");
            usingpanel_prop = serializedObject.FindProperty("usingPanel");
            paneldialog_prop = serializedObject.FindProperty("PanelDialog");
            shutdownobject_prop = serializedObject.FindProperty("ShutdownGameObject");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(enabled_prop, true);

            EditorGUILayout.PropertyField(enum_Condition);

            EditorGUILayout.PropertyField(enum_Status);

            GlobalVariable.CVariableType st = (GlobalVariable.CVariableType)enum_Status.enumValueIndex;

            switch (st)
            {
                case GlobalVariable.CVariableType.timeVar:
                    EditorGUILayout.PropertyField(timeVariables_prop, new GUIContent("Time Variables"));
                    EditorGUILayout.PropertyField(timeValue_prop, new GUIContent("Time Value"));
                    break;
                case GlobalVariable.CVariableType.healthVar:
                    EditorGUILayout.PropertyField(healthVariables_prop, new GUIContent("Health Variables"));
                    EditorGUILayout.PropertyField(healthValue_prop, new GUIContent("Health Value"));
                    break;
                case GlobalVariable.CVariableType.scoreVar:
                    EditorGUILayout.PropertyField(scoreVariables_prop, new GUIContent("Score Variables"));
                    EditorGUILayout.PropertyField(scoreValue_prop, new GUIContent("Score Value"));
                    break;
                case GlobalVariable.CVariableType.intVar:
                    EditorGUILayout.PropertyField(intVariables_prop, new GUIContent("Int Variables"));
                    EditorGUILayout.PropertyField(intValue_prop, new GUIContent("Int Value"));
                    break;
                case GlobalVariable.CVariableType.floatVar:
                    EditorGUILayout.PropertyField(floatVariables_prop, new GUIContent("Float Variables"));
                    EditorGUILayout.PropertyField(floatValue_prop, new GUIContent("Float Value"));
                    break;
                case GlobalVariable.CVariableType.stringVar:
                    EditorGUILayout.PropertyField(stringVariables_prop, new GUIContent("String Variables"));
                    EditorGUILayout.PropertyField(stringValue_prop, new GUIContent("String Value"));
                    break;
                case GlobalVariable.CVariableType.boolVar:
                    EditorGUILayout.PropertyField(boolVariables_prop, new GUIContent("Bool Variables"));
                    EditorGUILayout.PropertyField(boolValue_prop, new GUIContent("Bool Value"));
                    break;
            }

            EditorGUILayout.PropertyField(usingcanvas_prop, new GUIContent("Using Canvas"));
            EditorGUILayout.PropertyField(canvasdialog_prop, new GUIContent("Canvas Dialog"));
            EditorGUILayout.PropertyField(usingpanel_prop, new GUIContent("Using Panel"));
            EditorGUILayout.PropertyField(paneldialog_prop, new GUIContent("Panel Dialog"));

            EditorGUILayout.PropertyField(delayExecute_prop, new GUIContent("Delay Execute"));
            EditorGUILayout.PropertyField(shutdownobject_prop, true);

            serializedObject.ApplyModifiedProperties();
        }
    }
}