﻿/**************************************************************************************************************
 * Author : Rickman Roedavan
 * Version: 2.12
 * Desc   : Script percobaan testing gesture pada android. 
 **************************************************************************************************************/

 using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchPinch : MonoBehaviour {

    public bool isEnabled;

    public Camera TargetCamera;
    public float ScrollSpeed;

    [Header("ZoomIn Event")]
    public bool usingZoomInEvent;
    public UnityEvent ExecuteZoomInFunction;

    [Header("ZoomOut Event")]
    public bool usingZoomOutEvent;
    public UnityEvent ExecuteZoomOutFunction;

    [Header("Debug Value")]
    public bool showDebug;
    public int FingerTouchCount;
    public float FingerDelta;
    [Space(10)]
    public float Finger1TouchX;
    public float Finger1TouchY;
    public float Finger2TouchX;
    public float Finger2TouchY;
    [Space(10)]
    public string PinchStatus;
    float prevFingerDelta;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (isEnabled)
        {
            FingerTouchCount = Input.touchCount;

            if (Input.touchCount == 2)
            {
                Touch finger1Touch = Input.GetTouch(0);
                Touch finger2Touch = Input.GetTouch(1);

                if (finger1Touch.phase == TouchPhase.Moved && finger2Touch.phase == TouchPhase.Moved)
                {
                    Finger1TouchX = finger1Touch.position.x;
                    Finger1TouchY = finger1Touch.position.y;
                    Finger2TouchX = finger2Touch.position.x;
                    Finger2TouchY = finger2Touch.position.y;

                    Vector2 currDist = finger1Touch.position - finger2Touch.position;
                    Vector2 prevDist = ((finger1Touch.position - finger1Touch.position) - (finger2Touch.position - finger2Touch.deltaPosition));
                    FingerDelta = currDist.magnitude - prevDist.magnitude;

                    if (FingerDelta > prevFingerDelta)
                    {
                        PinchStatus = "ZoomIn";

                        if (TargetCamera.orthographic)
                        {
                            if (TargetCamera.orthographicSize > 10)
                            {
                                TargetCamera.orthographicSize -= 1 * ScrollSpeed;
                            }
                        }
                        else
                        {
                            TargetCamera.transform.Translate(Vector3.forward * ScrollSpeed * Time.deltaTime);
                        }

                        if (usingZoomInEvent)
                        {
                            ExecuteZoomInFunction.Invoke();
                        }
                    }
                    else
                    {
                        PinchStatus = "ZoomOut";

                        if (TargetCamera.orthographic)
                        {
                            TargetCamera.orthographicSize += 1 * ScrollSpeed;
                        }
                        else
                        {
                            TargetCamera.transform.Translate(Vector3.back * ScrollSpeed * Time.deltaTime);
                        }

                        if (usingZoomOutEvent)
                        {
                            ExecuteZoomOutFunction.Invoke();
                        }
                    }

                    prevFingerDelta = FingerDelta;

                }

            }

        }
    }

    public void TestingFunction()
    {
        Debug.Log("Finger Pinch Function Executed!");
    }

    void OnGUI()
    {
        if (showDebug)
        {
            GUILayout.BeginVertical();
            GUILayout.Label("FingerTouchCount: " + FingerTouchCount.ToString());
            GUILayout.Label("Finger1TouchX: " + Finger1TouchX.ToString());
            GUILayout.Label("Finger1TouchY: " + Finger1TouchY.ToString());
            GUILayout.Label("Finger2TouchX: " + Finger2TouchX.ToString());
            GUILayout.Label("Finger2TouchY: " + Finger2TouchY.ToString());
            GUILayout.Label("PinchStatus: " + PinchStatus);
            GUILayout.EndVertical();
        }
    }
}
