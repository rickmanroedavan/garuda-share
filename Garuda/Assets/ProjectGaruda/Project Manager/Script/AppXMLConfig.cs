﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

namespace IMedia9 {

    public class AppXMLConfig : MonoBehaviour {

        [Header("Asset Bundle Settings")]
        public string AssetsDataName;
        [HideInInspector]
        public GameObject AssetsDataObject;

        [Header("XML Settings")]
        public string AssetsXMLName;
        public GameObject GUILock;
        public GameObject ButtonFilename;
        public GameObject ButtonParent;

        [HideInInspector]
        public GameObject AssetsXMLObject;

        [Header("Scenario Settings")]
        public bool usingScenario;
        public GameObject ScenarioPanel;
        public InputField UserText;
        public InputField ScenarioText;

        [Header("SaveXML Settings")]
        public bool usingSaveXML;
        public GameObject SaveXMLPanel;
        public InputField SaveXMLText;

        [Header("LoadXML Settings")]
        public bool usingLoadXML;
        public GameObject LoadXMLPanel;
        public InputField LoadXMLText;

        public string DataDirectory()
        {
            return Application.streamingAssetsPath + "/Data/";
        }

        // Use this for initialization
        void Start() {

            AssetsDataObject = GameObject.Find(AssetsDataName);

            if (usingScenario) {
            }

            if (usingSaveXML)
            {
                AssetsXMLObject = GameObject.Find(AssetsXMLName);
            }
            if (usingLoadXML)
            {
                AssetsXMLObject = GameObject.Find(AssetsXMLName);
            }
        }

        // Update is called once per frame
        void Update() {

        }

        public void ShowScenarioPanel()
        {
            if (usingScenario)
            {
                GUILock.SetActive(true);
                ScenarioPanel.SetActive(true);
                GUILock.transform.SetAsLastSibling();
                ScenarioPanel.transform.SetAsLastSibling();

            }
        }

        public void CloseScenarioPanel()
        {
            if (usingScenario)
            {
                GUILock.SetActive(false);
                ScenarioPanel.SetActive(false);
            }
        }

        public void ShowSaveXMLPanel()
        {
            if (usingSaveXML)
            {
                GUILock.SetActive(true);
                SaveXMLPanel.SetActive(true);
                GUILock.transform.SetAsLastSibling();
                SaveXMLPanel.transform.SetAsLastSibling();

                DirectoryInfo tempDir;
                FileInfo[] tempFile = null;
                if (Directory.Exists(DataDirectory()))
                {
                    tempDir = new DirectoryInfo(DataDirectory());
                    tempFile = tempDir.GetFiles();
                }

                for (int i = 0; i < ButtonParent.transform.childCount; i++)
                {
                    Destroy(ButtonParent.transform.GetChild(i).gameObject);
                }

                for (int i = 0; i < tempFile.Length; i++)
                {
                    string temp = Path.GetExtension(tempFile[i].FullName);
                    if (temp == ".xml")
                    {
                        GameObject tempButton = Instantiate(ButtonFilename, ButtonParent.transform);
                        string[] nameOnly = tempFile[i].Name.Split('.');
                        tempButton.GetComponentInChildren<Text>().text = nameOnly[0];

                        Vector2 contentWidth = ButtonParent.GetComponent<RectTransform>().sizeDelta;
                        contentWidth.y += 50;
                        ButtonParent.GetComponent<RectTransform>().sizeDelta = contentWidth;

                    }
                }

            }
        }

        public void CloseSaveXMLPanel()
        {
            if (usingSaveXML)
            {
                GUILock.SetActive(false);
                SaveXMLPanel.SetActive(false);
            }
        }

        public void ShowLoadXMLPanel()
        {
            if (usingLoadXML)
            {
                GUILock.SetActive(true);
                LoadXMLPanel.SetActive(true);
                GUILock.transform.SetAsLastSibling();
                LoadXMLPanel.transform.SetAsLastSibling();

                DirectoryInfo tempDir;
                FileInfo[] tempFile = null;
                if (Directory.Exists(DataDirectory()))
                {
                    tempDir = new DirectoryInfo(DataDirectory());
                    tempFile = tempDir.GetFiles();
                }

                for (int i = 0; i < ButtonParent.transform.childCount; i++)
                {
                    Destroy(ButtonParent.transform.GetChild(i).gameObject);
                }

                for (int i = 0; i < tempFile.Length; i++)
                {
                    string temp = Path.GetExtension(tempFile[i].FullName);
                    if (temp == ".xml")
                    {
                        GameObject tempButton = Instantiate(ButtonFilename, ButtonParent.transform);
                        string[] nameOnly = tempFile[i].Name.Split('.');
                        tempButton.GetComponentInChildren<Text>().text = nameOnly[0];

                        Vector2 contentWidth = ButtonParent.GetComponent<RectTransform>().sizeDelta;
                        contentWidth.y += 50;
                        ButtonParent.GetComponent<RectTransform>().sizeDelta = contentWidth;
                    }
                }
            }
        }

        public void CloseLoadXMLPanel()
        {
            if (usingLoadXML)
            {
                GUILock.SetActive(false);
                LoadXMLPanel.SetActive(false);
            }
        }

        public void SetScenario()
        {
            if (usingScenario)
            {
                GlobalID.UserName = UserText.text;
                GlobalID.UserDesc = ScenarioText.text;
                AssetsDataObject.GetComponent<AssetBundleLoader>().ClearAllAssets();
            }
        }

        public void SetSaveXML()
        {
            if (usingSaveXML)
            {
                GlobalID.UserName = SaveXMLText.text;
                AssetsDataObject.GetComponent<AssetBundleLoader>().ClearAllAssets();
            }
        }

        public void SaveScenario()
        {
            if (usingSaveXML)
            {
                AssetsXMLObject.GetComponent<XMLManager>().SaveDataXML(SaveXMLText.text);
                CloseSaveXMLPanel();
                MenuStatusCaption.SetStatusBarCaption("File " + SaveXMLText.text + " telah di simpan.");
            }
        }

        public void LoadScenario()
        {
            if (usingLoadXML)
            {
                AssetsXMLObject.GetComponent<XMLManager>().LoadDataXML(LoadXMLText.text);
                CloseLoadXMLPanel();
                MenuStatusCaption.SetStatusBarCaption("File " + LoadXMLText.text + " telah di simpan.");
            }
        }

        public void SetCaptionButtonToText(GameObject thisButton)
        {
            SaveXMLText.text = thisButton.GetComponentInChildren<Text>().text;
        }

    }
}
