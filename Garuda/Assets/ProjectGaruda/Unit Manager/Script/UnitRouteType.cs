﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IMedia9
{
    public class UnitRouteType : MonoBehaviour
    {
        public GameObject ParentObject;
        public GameObject RingRoute;
        public int RouteType;

        [Header("Route Settings")]
        public Material RouteDefault;
        public Material RouteManuver;
        public Material RouteParachute;
        public Material RouteShooting;

        int currentType = 1;

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            RaycastHit hitInfo;
            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                if (ParentObject.GetComponent<UnitManager>().ObjectCamera.MainCamera.depth == 1)
                {
                    if (Physics.Raycast(ParentObject.GetComponent<UnitManager>().ObjectCamera.MainCamera.ScreenPointToRay(Input.mousePosition), out hitInfo)) 
                    {
                        if (hitInfo.collider.gameObject.name == this.gameObject.name)
                        {
                            ChangeRouteType();
                        }
                    }
                }
                else if (ParentObject.GetComponent<UnitManager>().ObjectCamera.SecondaryCamera.depth == 1)
                {
                    if (Physics.Raycast(ParentObject.GetComponent<UnitManager>().ObjectCamera.SecondaryCamera.ScreenPointToRay(Input.mousePosition), out hitInfo))
                    {
                        if (hitInfo.collider.gameObject.name == this.gameObject.name)
                        {
                            ChangeRouteType();
                        }
                    }
                }
            }
        }

        void LateUpdate()
        {
            if (Input.GetKeyUp(KeyCode.Tab))
            {
                SetRouteType(currentType);
                currentType++;
                if (currentType > 4) currentType = 1;
            }
        }

        public void ChangeRouteType()
        {
            switch (currentType)
            {
                case 1:
                    RingRoute.GetComponent<Renderer>().material = RouteDefault;
                    break;
                case 2:
                    RingRoute.GetComponent<Renderer>().material = RouteManuver;
                    break;
                case 3:
                    RingRoute.GetComponent<Renderer>().material = RouteParachute;
                    break;
                case 4:
                    RingRoute.GetComponent<Renderer>().material = RouteShooting;
                    break;
            }
            currentType++;
            if (currentType > 4) currentType = 1;
        }

        public void SetRouteType(int aValue)
        {
            switch (aValue)
            {
                case 1:
                    RingRoute.GetComponent<Renderer>().material = RouteDefault;
                    break;
                case 2:
                    RingRoute.GetComponent<Renderer>().material = RouteManuver;
                    break;
                case 3:
                    RingRoute.GetComponent<Renderer>().material = RouteParachute;
                    break;
                case 4:
                    RingRoute.GetComponent<Renderer>().material = RouteShooting;
                    break;
            }
        }
    }
}
