﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitBrowser : MonoBehaviour
{

    public GameObject PanelUnitBrowser;

    [Header("Dynamic Settings")]
    public InputField txtMovementType;
    public InputField txtMoveSpeed;
    public InputField txtRotationSpeed;
    public InputField txtGravity;
    public InputField txtScaleValue;

    [Header("Object Settings")]
    public InputField txtPositionX;
    public InputField txtPositionY;
    public InputField txtPositionZ;
    public InputField txtRotationX;
    public InputField txtRotationY;
    public InputField txtRotationZ;
    public InputField txtScaleX;
    public InputField txtScaleY;
    public InputField txtScaleZ;

    [Header("UID Settings")]
    public InputField txtUID;
    public InputField txtUsername;
    public InputField txtObjectname;
    public InputField txtPostfix;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowUnitBrowser()
    {
        PanelUnitBrowser.SetActive(true);
        this.transform.position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        this.transform.SetAsFirstSibling();
    }
}
