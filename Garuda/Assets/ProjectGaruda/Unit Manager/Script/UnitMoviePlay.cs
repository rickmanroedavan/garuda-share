﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class UnitMoviePlay : MonoBehaviour
{

    public string videoPanelName;
    [HideInInspector]
    public GameObject videoPanelObject;
    [HideInInspector]
    public VideoPlayer videoPanel; 

    // Start is called before the first frame update
    void Start()
    {
        videoPanelObject = GameObject.Find(videoPanelName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayVideo(Button VideoNameFromButton) {

        videoPanelObject.transform.GetChild(0).gameObject.SetActive(true);
        videoPanel = videoPanelObject.GetComponentInChildren<VideoPlayer>();

        string urlMovie = "file://" + Application.streamingAssetsPath + "/Movie/" + VideoNameFromButton.GetComponentInChildren<Text>().text;
        videoPanel.Stop();
        videoPanel.url = urlMovie;
        videoPanel.Play();
    }

    public void StopVideo()
    {
        videoPanel.Stop();
    }

}
