﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class UnitMovieMenu   : MonoBehaviour
{
    public bool isEnabled;
    public GameObject MovieButton;
    public GameObject MovieButtonParent;

    [Header("Video GameObject")]
    public GameObject VideoPlayerObject;

    [Header("Extension Settings")]
    public string[] FileExtension;

    // Start is called before the first frame update
    void Start()
    {
        VideoPlayerObject.SetActive(false); 
    }

    public bool isValid(string aFileExt)
    {
        bool result = false;
        for (int i = 0; i < FileExtension.Length; i++)
        {
            if (FileExtension[i] == aFileExt)
            {
                result = true;
            }
        }
        return result;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string MovieDirectory()
    {
        return Application.streamingAssetsPath + "/Movie/";
    }

    public void CloseMovie()
    {
        VideoPlayerObject.GetComponent<VideoPlayer>().Stop();
        VideoPlayerObject.SetActive(false);
    }

    public void ShowMovieFile()
    {
        if (isEnabled)
        {

            DirectoryInfo tempDir;
            FileInfo[] tempFile = null;
            if (Directory.Exists(MovieDirectory()))
            {
                tempDir = new DirectoryInfo(MovieDirectory());
                tempFile = tempDir.GetFiles();
            }

            for (int i = 0; i < MovieButtonParent.transform.childCount; i++) {
                Destroy(MovieButtonParent.transform.GetChild(i).gameObject);
            }

            Vector2 contentWidth = new Vector2(0, 100); // MovieButtonParent.GetComponent<RectTransform>().sizeDelta;

            for (int i = 0; i < tempFile.Length; i++)
            {
                string temp = Path.GetExtension(tempFile[i].FullName);
                if (isValid(temp))
                {
                    GameObject tempButton = Instantiate(MovieButton, MovieButtonParent.transform);
                    tempButton.GetComponentInChildren<Text>().text = tempFile[i].Name;

                    contentWidth.y += 50;
                    MovieButtonParent.GetComponent<RectTransform>().sizeDelta = contentWidth;
                }
            }

        }
    }
}
