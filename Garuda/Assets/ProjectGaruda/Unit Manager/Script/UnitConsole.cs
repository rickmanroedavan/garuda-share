﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IMedia9
{

    public class UnitConsole : MonoBehaviour
    {

        public GameObject PanelDebug;
        public GameObject PanelText;
        public static string DebugText = "";

        // Use this for initialization
        void Start()
        {
            PanelDebug.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyUp(KeyCode.F12))
            {
                if (PanelDebug.activeSelf) PanelDebug.SetActive(false);
                else PanelDebug.SetActive(true);
            }
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                PanelDebug.SetActive(false);
            }
            if (PanelDebug.activeSelf)
            {
                PanelText.GetComponent<Text>().text = DebugText;
            }
        }

        public static void PrintDebug(string aText)
        {
            if (DebugText.Length > 5000) DebugText = "";
            string texts = System.DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " : " + aText + "\n";
            DebugText = texts + DebugText;
        }
    }
}
