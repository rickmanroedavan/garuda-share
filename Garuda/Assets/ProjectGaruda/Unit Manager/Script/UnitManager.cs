﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace IMedia9
{

    public class UnitManager : MonoBehaviour
    {
        public enum CParameterType { Int, Float, Bool, Trigger }

        [Header("Asset Bundle Settings")]
        public string AssetsDataName;
        [HideInInspector]
        public GameObject AssetsDataObject;

        [System.Serializable]
        public class CObjectUID
        {
            public string UID;
            public string Username;
            public string ObjectName;
            public string Postfix;
        }
        [Header("UID Settings")]
        public CObjectUID ObjectUID;

        //------------------------------------------------------------------------------------------- CAMERA SETTINGS
        [System.Serializable]
        public class CCameraSettings
        {
            public bool isEnabled;
            [Space(10)]

            public string MainCameraName;
            public string SecondaryCameraName;

            [HideInInspector]
            public Camera MainCamera;
            [HideInInspector]
            public Camera SecondaryCamera;

            [Header("Camera Focus Settings")]
            public Camera CameraFocusObject;

            [Header("Touch Settings")]
            public float perspectiveZoomSpeed = 0.5f;
            public float orthoZoomSpeed = 0.5f;

            [Header("Debug Value")]
            public bool isMainCamera;
            public bool isSecondaryCamera;

        }

        [Header("Camera Settings")]
        public CCameraSettings ObjectCamera;

        //------------------------------------------------------------------------------------------- TERRAIN SETTINGS
        [System.Serializable]
        public class CTerrainSettings
        {
            public bool isEnabled;
            [Space(10)]

            [Header("Tag Settings")]
            public string GroundTag;
            public string TerrainTag;
            public string OceanTag;
            public string Map2DTag;
            public string Map3DTag;
        }

        [Header("Terrain Settings")]
        public CTerrainSettings ObjectTerrain;

        //------------------------------------------------------------------------------------------- OBJECT SETTINGS
        [System.Serializable]
        public class CObjectSettings
        {
            public bool isEnabled;
            [Space(10)]

            [Header("TargetObject Settings")]
            public string ControllerTag;
            public GameObject TargetController;
            public BoxCollider TargetCollider;

            [Header("Rigidbody Settings")]
            public bool usingRigidbody;
            public Rigidbody TargetRigidbody;

            [Header("Object Selection Settings")]
            public bool usingObjectSelection;
            public bool ObjectSelectionStatus;
            public GameObject ObjectSelection;

            [Header("Route Selection Settings")]
            public bool usingRouteSelection;
            public bool RouteSelectionStatus;
            public GameObject RouteSelection;
            public GameObject RingSelection;
        }

        [Header("Object Settings")]
        public CObjectSettings ObjectTarget;

        [System.Serializable]
        public class CObjectMovement
        {
            //------------------------------------------------------------------------------------------- MOVEMENT SETTINGS
            public enum CMovementType { TranslateMode, TransformMode }

            public bool isEnabled;
            [Space(10)]

            [Header("Movement Settings")]
            public CMovementType MovementType;
            public float MoveSpeed = 1;
            public float RotateSpeed = 5;
            public float ScaleValue = 5;
            public float Gravity = 20.0F;
            [HideInInspector]
            public Vector3 moveDirection = Vector3.zero;

            [Header("Movement Keys")]
            public KeyCode ForwardKey = KeyCode.UpArrow;
            public KeyCode LeftKey = KeyCode.LeftArrow;
            public KeyCode BackKey = KeyCode.DownArrow;
            public KeyCode RightKey = KeyCode.RightArrow;
            public KeyCode UpKey = KeyCode.Home;
            public KeyCode DownKey = KeyCode.End;
            public KeyCode RotateUpKey = KeyCode.PageUp;
            public KeyCode RotateDownKey = KeyCode.PageDown;
            public KeyCode RollUpKey = KeyCode.Insert;
            public KeyCode RollDownKey = KeyCode.Delete;
            [Space(10)]

            [Header("Alternative Keys")]
            public KeyCode AltForwardKey = KeyCode.None;
            public KeyCode AltLeftKey = KeyCode.None;
            public KeyCode AltBackKey = KeyCode.None;
            public KeyCode AltRightKey = KeyCode.None;
            public KeyCode AltUpKey = KeyCode.None;
            public KeyCode AltDownKey = KeyCode.None;
            public KeyCode AltRotateUpKey = KeyCode.None;
            public KeyCode AltRotateDownKey = KeyCode.None;
            public KeyCode AltRollUpKey = KeyCode.None;
            public KeyCode AltRollDownKey = KeyCode.None;

            [Header("Sound Settings")]
            public bool usingSound;
            public AudioSource SoundObject;
            public AudioClip SoundClip;

            [Header("Target Object Status")]
            public bool isMovingStatus;
            public bool isMovingRoute;
            public bool isSelected;
            public bool isRouteSelected;
            public bool isFollowMouse;
            public bool isRotateObject;
            public bool isGround;
            public bool isOcean;

            //-- Private
            [HideInInspector]
            public int CurrentRouteIndex;
            [HideInInspector]
            public Ray Raycast;
            [HideInInspector]
            public float RaycastDistance = 10000;
            [HideInInspector]
            public Vector3 RaycastOffset;
            [HideInInspector]
            public RaycastHit Raycasthit;
        }

        [Header("Movement Settings")]
        public CObjectMovement ObjectMovement;

        //------------------------------------------------------------------------------------------- HUMAN SETTINGS

        [System.Serializable]
        public class CHumanSetting
        {
            [System.Serializable]
            public class CMovingState3D
            {
                public string StateNow;
                public string StateNext;
                public CParameterType ParameterType;
                public string ParameterName;
                public string PositiveValue;
                public string NegativeValue;
                public KeyCode[] TriggerKey;
                public bool ForceAnimation;
                [Header("Sound Settings")]
                public bool usingSound;
                public AudioSource animaAudioSource;
                public AudioClip animaAudioClip;
            }

            [System.Serializable]
            public class CActionState3D
            {
                public string StateNow;
                public string StateNext;
                public CParameterType ParameterType;
                public string ParameterName;
                public string PositiveValue;
                public string NegativeValue;
                public KeyCode[] TriggerKey;
                public bool ForceAnimation;
                [Header("Sound Settings")]
                public bool usingSound;
                public AudioSource animaAudioSource;
                public AudioClip animaAudioClip;
            }

            [System.Serializable]
            public class CShutdownState3D
            {
                public string StateNow;
                public string StateNext;
                public CParameterType ParameterType;
                public string ParameterName;
                public string PositiveValue;
                public string NegativeValue;
                public KeyCode[] ShutdownTestkey;
            }

            public bool isEnabled;

            [Header("Animator Settings")]
            public bool usingAnimator;
            public Animator TargetAnimator;
            public GameObject TargetWeapon;
            public GameObject TargetParachute;

            [Header("Movement Settings")]
            public bool usingMovingState;
            public CMovingState3D[] MovingState3D;

            [Header("Action Settings")]
            public bool usingActionState;
            public CActionState3D[] ActionState3D;

            [Header("Shutdown Settings")]
            public bool usingShutdownState;
            public CShutdownState3D ShutdownState3D;

        }

        [Header("Human Settings")]
        public CHumanSetting ObjectHuman;

        //------------------------------------------------------------------------------------------- AIRCRAFT SETTINGS

        [System.Serializable]
        public class CAircraftSetting
        {
            public GameObject VisualObject;

            [Header("LandingGear Settings")]
            public bool usingLandingGear;
            public float LandingHeight;
            public GameObject[] LandingObject;

            [Header("Rotor Settings")]
            public bool usingRotor;
            public float RotorSpeed;
            public GameObject[] RotorObject;
            public Vector3[] RotateDirection;

            [Header("SFX Selection Settings")]
            public bool usingSFXSelection;
            public bool SFXSelectionStatus;
            public GameObject[] SFXSelection;

            [Header("Debug Value")]
            public bool statusRotor;
            public bool statusLandingGear;

        }

        [System.Serializable]
        public class CAircraftObject
        {
            public bool isEnabled;
            [Space(10)]
            public CAircraftSetting[] AircraftSettings;
        }

        [Header("Aircraft Settings")]
        public CAircraftObject ObjectAircraft;

        //------------------------------------------------------------------------------------------- VEHICLE SETTINGS

        [System.Serializable]
        public class CVehicleSetting
        {
            public GameObject VisualObject;

            [Header("Turret Horizontal Settings")]
            public bool usingRotationHorizontal;
            public GameObject TurretObjectH;
            public KeyCode RotateLeft;
            public KeyCode RotateRight;

            [Header("Turret Vertical Settings")]
            public bool usingRotationVertical;
            public GameObject TurretObjectV;
            public KeyCode RotateUp;
            public KeyCode RotateDown;

            [Header("SFX Selection Settings")]
            public bool usingSFXSelection;
            public bool SFXSelectionStatus;
            public GameObject[] SFXSelection;

        }

        [System.Serializable]
        public class CVehicleObject
        {
            public bool isEnabled;
            [Space(10)]
            public CVehicleSetting[] VehicleSettings;
        }

        [Header("Vehicle Settings")]
        public CVehicleObject ObjectVehicle;

       //------------------------------------------------------------------------------------------- ACTION SETTINGS

        [System.Serializable]
        public class CActionSetting
        {
            public bool isEnabled;
            [Space(10)]

            [Header("Trooper Settings")]
            public bool usingTrooper;
            public KeyCode TrooperKey;
            public GameObject HumanObjectPosition;
            public string HumanObject;

            [Header("Weapon Settings")]
            public bool usingWeapon;
            public KeyCode WeaponKey;
            [Space(10)]
            public bool usingCenterWeapon;
            public GameObject CenterWeaponPosition;
            public string CenterWeaponObject;
            [Space(10)]
            public bool usingLeftWeapon;
            public GameObject LeftWeaponPosition;
            public string LeftWeaponObject;
            [Space(10)]
            public bool usingRightWeapon;
            public GameObject RightWeaponPosition;
            public string RightWeaponObject;

            [Header("Gun Settings")]
            public bool usingGun;
            public GameObject GunPosition;
            public GameObject GunSFX;

            [Header("Sound Settings")]
            public bool usingSound;
            public AudioSource GunAudioSource;
            public AudioClip GunAudioClip;
            public string GunObject;

        }

        [Header("Weapon Settings")]
        public CActionSetting[] ObjectAction;

        //------------------------------------------------------------------------------------------- ROUTE SETTINGS

        [System.Serializable]
        public class CUIRouteType
        {
            public string RouteType;
            public string RoutePositionX;
            public string RoutePositionY;
            public string RoutePositionZ;
            public string UsingChildObject;
        }

        [System.Serializable]
        public class CRouteSetting
        {
            public string RouteInfo;
            public Vector3 RoutePoint;
            public GameObject RouteObject;
        }

        [System.Serializable]
        public class CRouteObject
        {
            public bool isEnabled;
            public Vector3 StartPosition;
            public List<CRouteSetting> RouteSetting;
        }

        [Header("Route Settings")]
        public CRouteObject ObjectRoute;

        //------------------------------------------------------------------------------------------- XML SETTINGS

        [System.Serializable]
        public class CXMLSetting
        {
            public bool isEnabled;
            [Space(10)]

            [Header("Canvas Settings")]
            public string CanvasParent;
            public KeyCode TriggerKey;
            public string UnitPanelName;
            [HideInInspector]
            public GameObject UnitPanelObject;

            [Header("User Settings")]
            public string UID;
            public string Username;
            public string ObjectName;
            public string Postfix;

            [Header("Object Settings")]
            public string TargetPositionX;
            public string TargetPositionY;
            public string TargetPositionZ;
            public string TargetRotationX;
            public string TargetRotationY;
            public string TargetRotationZ;
            public string TargetScaleX;
            public string TargetScaleY;
            public string TargetScaleZ;

            [Header("Dynamic Settings")]
            public string MovementType;
            public string ScaleValue;
            public string MoveSpeed;
            public string RotateSpeed;
            public string Gravity;

            [Header("Route Settings")]
            public string TotalRoute;
            public CUIRouteType[] RouteType;
        }

        [Header("XML Settings")]
        public CXMLSetting ObjectXML;

        //-- PRIVATE VARIABLES
        Vector3 detectPosition;
        Vector3 ScreenPoint;
        Vector3 Offset;
        float rotorSpeed;
        bool isCooldown = false;

        public float manuverTime = 1.0f;
        public float manuverSpeed;

        float manuverStartTime;
        Vector3 manuverCenterPoint;
        Vector3 manuverStartRelCenter;
        Vector3 manuverEndRelCenter;

        Transform ManuverStartPos;
        Transform ManuverEndPos;

        //******************************************************************************************************
        void Start()
        {
            ObjectMovement.isGround = false;
            ObjectRoute.RouteSetting = new List<CRouteSetting>();
            AssetsDataObject = GameObject.Find(AssetsDataName);

            if (ObjectHuman.TargetWeapon != null) ObjectHuman.TargetWeapon.SetActive(false);
            if (ObjectTarget.ObjectSelection != null) ObjectTarget.ObjectSelection.SetActive(false);

            if (ObjectTarget.isEnabled && ObjectTarget.usingRigidbody)
            {
                if (ObjectTarget.TargetRigidbody != null) ObjectTarget.TargetRigidbody.useGravity = true;
                if (ObjectTarget.TargetCollider != null) ObjectTarget.TargetCollider.isTrigger = false;
            } else
            {
                ObjectMovement.Gravity = 0;
                if (ObjectTarget.TargetRigidbody != null) ObjectTarget.TargetRigidbody.useGravity = false;
                if (ObjectTarget.TargetCollider != null) ObjectTarget.TargetCollider.isTrigger = true;
            }
            if (!ObjectCamera.isMainCamera)
            {
                if (GameObject.Find(ObjectCamera.MainCameraName) != null)
                {
                    ObjectCamera.MainCamera = GameObject.Find(ObjectCamera.MainCameraName).GetComponent<Camera>();
                    ObjectCamera.isMainCamera = true;
                }
            }
            if (!ObjectCamera.isSecondaryCamera)
            {
                if (GameObject.Find(ObjectCamera.SecondaryCameraName) != null)
                {
                    ObjectCamera.SecondaryCamera = GameObject.Find(ObjectCamera.SecondaryCameraName).GetComponent<Camera>();
                    ObjectCamera.isSecondaryCamera = true;
                }
            }

            //for (int i = 0; i < ObjectAircraft.Length; i++)
            //{
                for (int i = 0; i < ObjectAircraft.AircraftSettings.Length; i++)
                {
                    if (ObjectAircraft.AircraftSettings[i].usingSFXSelection)
                    {
                        for (int j = 0; j < ObjectAircraft.AircraftSettings[i].SFXSelection.Length; j++)
                        {
                            ObjectAircraft.AircraftSettings[i].SFXSelection[j].SetActive(false);
                        }
                    }
                }
            //}

            //ObjectXML.UnitPanel.SetActive(false);
            ObjectCamera.CameraFocusObject.gameObject.SetActive(false);
        }

        //******************************************************************************************************
        void FixedUpdate()
        {
            //-- ANIMATION SETTINGS
            SetArmyAnimation();
            SetAircraftRotor();
            SetTankTurret();

            //-- WEAPON SETTINGS
            LaunchTroop();

            //-- DELETE OBJECT
            DeleteObject();
        }

        public void ResetAllObject()
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                GameObject.Find(ObjectCamera.SecondaryCameraName).GetComponent<CameraUFO>().ActivateCamera(true);

                ObjectCamera.CameraFocusObject.gameObject.SetActive(false);
                ObjectCamera.CameraFocusObject.depth = -1;

                ObjectMovement.isMovingStatus = false;

                ObjectMovement.isSelected = false;
                ObjectTarget.ObjectSelection.SetActive(false);

                ObjectMovement.isRouteSelected = false;
                ObjectTarget.RouteSelection.SetActive(false);
                ObjectTarget.RingSelection.SetActive(false);

                ObjectMovement.isMovingRoute = false;

                SetIdleArmyAnimation();

                SetAircraftSFX(false);
                SetVehicleSFX(false);
                ObjectMovement.SoundObject.Stop();
                CloseUnitPanel();
            }
        }

        void Update()
        {
            //-- RESET ALL OBJECT
            ResetAllObject();

            if (ObjectMovement.isSelected && Input.GetKeyUp(ObjectXML.TriggerKey))
            {
                GameObject canvasParent = GameObject.Find(ObjectXML.CanvasParent);
                if (canvasParent != null)
                {
                    //--Change
                    SyncronizeUnitPanel();
                    SyncronizeUnitPanelOnScreen();
                }
            }

            //-- OBJECT SOUND SETTINGS
            if (ObjectMovement.usingSound)
            {
                
            }

            //-- OBJECT INPUT SETTINGS
            if (ObjectMovement.isSelected && ObjectMovement.isEnabled && ObjectMovement.MovementType == CObjectMovement.CMovementType.TransformMode)
            {
                if (!ObjectMovement.isGround)
                {
                    ObjectMovement.moveDirection.y -= ObjectMovement.Gravity * Time.deltaTime;
                }

                if (Input.GetKey(ObjectMovement.ForwardKey) || Input.GetKey(ObjectMovement.AltForwardKey) || Input.GetKey(ObjectMovement.BackKey) || Input.GetKey(ObjectMovement.AltBackKey))
                {
                    ObjectMovement.moveDirection = new Vector3(GetAxisHorizontal(), 0, GetAxisVertical());
                    ObjectMovement.moveDirection = transform.TransformDirection(ObjectMovement.moveDirection);
                    ObjectMovement.moveDirection *= ObjectMovement.MoveSpeed;

                    ObjectTarget.TargetController.transform.position += ObjectMovement.moveDirection * Time.deltaTime;
                    ObjectMovement.isMovingStatus = true;
                }

                if (Input.GetKey(ObjectMovement.LeftKey) || Input.GetKey(ObjectMovement.AltLeftKey))
                {
                    transform.Rotate(0, -ObjectMovement.RotateSpeed, 0);
                    ObjectMovement.isMovingStatus = true;
                }

                if (Input.GetKey(ObjectMovement.RightKey) || Input.GetKey(ObjectMovement.AltRightKey))
                {
                    transform.Rotate(0, ObjectMovement.RotateSpeed, 0);
                    ObjectMovement.isMovingStatus = true;
                }

                if (Input.GetKey(ObjectMovement.RotateUpKey) || Input.GetKey(ObjectMovement.AltRotateUpKey))
                {
                    transform.Rotate(-ObjectMovement.RotateSpeed, 0, 0);
                    ObjectMovement.isMovingStatus = true;
                }

                if (Input.GetKey(ObjectMovement.RotateDownKey) || Input.GetKey(ObjectMovement.AltRotateDownKey))
                {
                    transform.Rotate(ObjectMovement.RotateSpeed, 0, 0);
                    ObjectMovement.isMovingStatus = true;
                }

            }
            else if (ObjectMovement.isSelected && ObjectMovement.isEnabled && ObjectMovement.MovementType == CObjectMovement.CMovementType.TranslateMode)
            {
                if (Input.GetKey(ObjectMovement.ForwardKey) || Input.GetKey(ObjectMovement.AltForwardKey))
                {
                    transform.Translate(Vector3.forward * ObjectMovement.MoveSpeed * Time.deltaTime);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.BackKey) || Input.GetKey(ObjectMovement.AltBackKey))
                {
                    transform.Translate(Vector3.back * ObjectMovement.MoveSpeed * Time.deltaTime);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.LeftKey) || Input.GetKey(ObjectMovement.AltLeftKey))
                {
                    transform.Rotate(0, -ObjectMovement.RotateSpeed, 0);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.RightKey) || Input.GetKey(ObjectMovement.AltRightKey))
                {
                    transform.Rotate(0, ObjectMovement.RotateSpeed, 0);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.RotateUpKey) || Input.GetKey(ObjectMovement.AltRotateUpKey))
                {
                    transform.Rotate(-ObjectMovement.RotateSpeed, 0, 0);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.RotateDownKey) || Input.GetKey(ObjectMovement.AltRotateDownKey))
                {
                    transform.Rotate(ObjectMovement.RotateSpeed, 0, 0);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.UpKey) || Input.GetKey(ObjectMovement.AltUpKey))
                {
                    transform.Translate(Vector3.up * ObjectMovement.MoveSpeed * Time.deltaTime);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.DownKey) || Input.GetKey(ObjectMovement.AltDownKey))
                {
                    transform.Translate(Vector3.down * ObjectMovement.MoveSpeed * Time.deltaTime);
                    ObjectMovement.isMovingStatus = true;
                }
                if (Input.GetKey(ObjectMovement.RollUpKey) || Input.GetKey(ObjectMovement.AltRollUpKey))
                {
                    transform.Rotate(0, 0, -ObjectMovement.RotateSpeed);
                    ObjectMovement.isMovingStatus = true;
                }

                if (Input.GetKey(ObjectMovement.RollDownKey) || Input.GetKey(ObjectMovement.AltRollDownKey))
                {
                    transform.Rotate(0, 0, ObjectMovement.RotateSpeed);
                    ObjectMovement.isMovingStatus = true;
                }
            }

            //-- OBJECT SOUND SETTINGS
            if (Input.GetKeyUp(ObjectMovement.UpKey) || Input.GetKeyUp(ObjectMovement.DownKey) || Input.GetKeyUp(ObjectMovement.LeftKey) || Input.GetKeyUp(ObjectMovement.RightKey) ||
                Input.GetKeyUp(ObjectMovement.AltUpKey) || Input.GetKeyUp(ObjectMovement.AltDownKey) || Input.GetKeyUp(ObjectMovement.AltLeftKey) || Input.GetKeyUp(ObjectMovement.AltRightKey))
            {
                ObjectMovement.isMovingStatus = false;
                ObjectMovement.SoundObject.Stop();
            }

            if (ObjectMovement.usingSound && ObjectMovement.isMovingStatus)
            {
                if (!ObjectMovement.SoundObject.isPlaying)
                {
                    ObjectMovement.SoundObject.Play();
                }
            }

            //-- SPECIAL CONDITION
            if (ObjectHuman.isEnabled)
            {
                if (ObjectHuman.TargetWeapon != null)
                {
                    ObjectHuman.TargetWeapon.SetActive(ObjectMovement.isGround);
                }
                if (ObjectMovement.isGround)
                {
                    ObjectHuman.TargetParachute.SetActive(false);
                    ObjectHuman.TargetAnimator.SetBool("isGround", true);
                } else
                {
                    ObjectHuman.TargetParachute.SetActive(true);
                    ObjectHuman.TargetAnimator.SetBool("isGround", false);
                }
            }

            if (ObjectTarget.usingObjectSelection && ObjectMovement.isSelected)
            {
                SetObjectSelection();
            }

            if (ObjectTarget.usingObjectSelection && ObjectMovement.isRouteSelected)
            {
                SetRouteSelection();
            }

            if (ObjectMovement.isSelected)
            {
                DetectHumanGround();
                SetAircraftSFX(true);
                SetVehicleSFX(true);
                SetAircraftLandingGear();
                SetAircraftRotor();
                LaunchTroop();
                LaunchWeapon();
            }

            MovingByRoute();
        }

        void DeleteObject()
        {
            if (Input.GetKeyUp(KeyCode.Delete))
            {
                if (ObjectMovement.isSelected)
                {
                    object args = new object();
                    args = this.gameObject.name;
                    AssetsDataObject.SendMessage("Shutdown", args);
                }
                if (ObjectMovement.isRouteSelected)
                {
                    for (int i = 1; i < this.transform.GetChild(1).transform.childCount; i++){
                        Destroy(this.transform.GetChild(1).transform.GetChild(i).gameObject);
                    }
                }
            }
        }


        void SetObjectSelection()
        {
            ObjectTarget.ObjectSelection.SetActive(true);
            ObjectTarget.ObjectSelection.transform.rotation = Quaternion.Euler(0, ObjectTarget.ObjectSelection.transform.rotation.eulerAngles.y, 0);
        }

        void SetRouteSelection()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && GlobalMouse.MouseType == GlobalMouse.CMouseType.MouseMap)
            {
                if (!ObjectTarget.RouteSelection.activeSelf)
                {
                    ObjectTarget.RouteSelection.SetActive(true);
                    ObjectTarget.RingSelection.SetActive(true);
                }
                else
                if (ObjectTarget.RouteSelection.activeSelf)
                {
                    Invoke("LaunchRoute", 0.5f);
                }
            }
        }

        void MovingByRoute()
        {
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.Return))
            {

                if (ObjectRoute.RouteSetting.Count > 0)
                {
                    ObjectMovement.isMovingRoute = true;
                    ObjectMovement.CurrentRouteIndex = 0;

                    this.transform.position = ObjectRoute.StartPosition;

                    ObjectTarget.RingSelection.SetActive(false);
                    for (int i = 0; i < ObjectRoute.RouteSetting.Count; i++)
                    {
                        ObjectRoute.RouteSetting[i].RouteObject.SetActive(false);
                    }

                    if (ObjectAircraft.isEnabled) SetAircraftSFX(true);
                    if (ObjectVehicle.isEnabled) SetVehicleSFX(true);
                }
            }

            if (ObjectMovement.isMovingRoute)
            {
                if (ObjectMovement.CurrentRouteIndex < ObjectRoute.RouteSetting.Count)
                {
                    Vector3 source = this.transform.position;
                    Vector3 dest = ObjectRoute.RouteSetting[ObjectMovement.CurrentRouteIndex].RoutePoint;
                    dest.y = source.y;
                    this.transform.LookAt(dest);
                    if (Vector3.Distance(source, dest) > 1)
                    {
                        if (ObjectHuman.isEnabled)
                        {
                            this.transform.position = Vector3.MoveTowards(this.transform.position, dest, ObjectMovement.MoveSpeed * Time.deltaTime);
                            SetArmyMovingAnimation();
                        }
                        else if (ObjectAircraft.isEnabled)
                        {
                            for (int i = 0; i < ObjectAircraft.AircraftSettings.Length; i++)
                            {
                                this.transform.position = Vector3.MoveTowards(this.transform.position, dest, ObjectMovement.MoveSpeed * Time.deltaTime);
                            }
                        }
                        else if (ObjectVehicle.isEnabled)
                        {
                            for (int i = 0; i < ObjectVehicle.VehicleSettings.Length; i++)
                            {
                                this.transform.position = Vector3.MoveTowards(this.transform.position, dest, ObjectMovement.MoveSpeed * Time.deltaTime);
                            }
                        }
                    }
                    else
                    {
                        ObjectMovement.CurrentRouteIndex++;
                    }
                }
            }
        }

        public void SetQuaternionManuver(Transform startPos, Transform endPos)
        {
            Vector3 direction = Vector3.right;

            manuverCenterPoint = (startPos.position + endPos.position) * .5f;
            manuverCenterPoint -= direction;
            manuverStartRelCenter = startPos.position - manuverCenterPoint;
            manuverEndRelCenter = endPos.position - manuverCenterPoint;

            float fracComplete = (Time.time - manuverStartTime) / manuverTime * manuverSpeed;

            transform.position = Vector3.Slerp(manuverStartRelCenter, manuverEndRelCenter, fracComplete * manuverSpeed);
            transform.position += manuverCenterPoint;
            transform.LookAt(manuverCenterPoint);
        }

        void LaunchRoute()
        {
            Vector3 mousePosition = GlobalMouse.VectorMouse;
            GameObject temp = Instantiate(ObjectTarget.RingSelection, mousePosition, ObjectTarget.RingSelection.transform.rotation, ObjectTarget.RouteSelection.transform);
            temp.transform.localScale = new Vector3(temp.transform.localScale.x,
                                                    temp.transform.localScale.y,
                                                    temp.transform.localScale.z);

            CRouteSetting newItem = new CRouteSetting();
            newItem.RouteInfo = "Route" + ObjectRoute.RouteSetting.Count.ToString();
            newItem.RoutePoint = mousePosition;
            newItem.RouteObject = temp;

            temp.gameObject.name = newItem.RouteInfo;

            ObjectRoute.StartPosition = this.transform.position;
            ObjectRoute.RouteSetting.Add(newItem);
        }

        void LaunchTroop()
        {
            for (int i = 0; i < ObjectAction.Length; i++)
            {
                if (ObjectAction[i].isEnabled && ObjectAction[i].usingTrooper)
                {
                    if (Input.GetKeyUp(ObjectAction[i].TrooperKey) && !isCooldown)
                    {
                        isCooldown = true;
                        LoadObjectByName(ObjectAction[i].HumanObject, ObjectAction[i].HumanObjectPosition);
                        Invoke("Cooldown", 1);
                    }
                }
            }
        }

        void LaunchWeapon()
        {
            for (int i = 0; i < ObjectAction.Length; i++)
            {
                if (ObjectAction[i].isEnabled && ObjectAction[i].usingWeapon)
                {
                    if (Input.GetKeyUp(ObjectAction[i].WeaponKey) && !isCooldown)
                    {
                        isCooldown = true;

                        if (ObjectAction[i].usingCenterWeapon)
                        {
                            LoadObjectByName(ObjectAction[i].CenterWeaponObject, ObjectAction[i].CenterWeaponPosition, ObjectAction[i].CenterWeaponPosition);
                        }
                        if (ObjectAction[i].usingLeftWeapon)
                        {
                            LoadObjectByName(ObjectAction[i].LeftWeaponObject, ObjectAction[i].LeftWeaponPosition, ObjectAction[i].LeftWeaponPosition);
                        }
                        if (ObjectAction[i].usingRightWeapon)
                        {
                            LoadObjectByName(ObjectAction[i].RightWeaponObject, ObjectAction[i].RightWeaponPosition, ObjectAction[i].LeftWeaponPosition);
                        }

                        Invoke("Cooldown", 1);
                    }
                }
                else if (ObjectAction[i].isEnabled && ObjectAction[i].usingGun)
                {
                    if (Input.GetKey(ObjectAction[i].WeaponKey))
                    {

                        ObjectAction[i].GunSFX.SetActive(true);

                        if (ObjectAction[i].usingSound)
                        {
                            ObjectAction[i].GunAudioSource.clip = ObjectAction[i].GunAudioClip;
                            if (!ObjectAction[i].GunAudioSource.isPlaying)
                            {
                                ObjectAction[i].GunAudioSource.Play();
                            }
                        }

                        if (!isCooldown)
                        {
                            isCooldown = true;

                            if (ObjectAction[i].usingGun)
                            {
                                LoadObjectByName(ObjectAction[i].GunObject, ObjectAction[i].GunPosition, ObjectAction[i].GunPosition);
                            }

                            Invoke("Cooldown", 0.1f);
                        }

                    } else if (Input.GetKeyUp(ObjectAction[i].WeaponKey))
                    {
                        ObjectAction[i].GunSFX.SetActive(false);
                        ObjectAction[i].GunAudioSource.Stop();
                    }

                }
            }
        }


        void Cooldown()
        {
            isCooldown = false;
        }

        void OnMouseDown()
        {
            if (GlobalMouse.MouseType == GlobalMouse.CMouseType.MouseSelect)
            {
                ObjectMovement.isSelected = true;
                UnitConsole.PrintDebug("UnitManager (805): Status Object Selected: " + ObjectMovement.isSelected);
            }
            if (GlobalMouse.MouseType == GlobalMouse.CMouseType.MouseMap && !ObjectMovement.isRouteSelected)
            {
                ObjectMovement.isRouteSelected = true;
                UnitConsole.PrintDebug("UnitManager (810): Status Map Point Ready: " + ObjectMovement.isRouteSelected);
            }
            if (GlobalMouse.MouseType == GlobalMouse.CMouseType.MouseSelectZoom)
            {
                ObjectMovement.isSelected = true;
                ObjectCamera.CameraFocusObject.gameObject.SetActive(true);
                ObjectCamera.CameraFocusObject.depth = 3;

                GameObject.Find(ObjectCamera.SecondaryCameraName).GetComponent<CameraUFO>().ActivateCamera(false);

                if (CameraSSD.IsMultiDisplay())
                {
                    ObjectCamera.CameraFocusObject.targetDisplay = ObjectCamera.SecondaryCamera.targetDisplay;
                }

                UnitConsole.PrintDebug("UnitManager (790): Status Object Selected: " + ObjectMovement.isSelected);
            }
            if (GlobalMouse.MouseType == GlobalMouse.CMouseType.MousePan)
            {
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                {
                    ScreenPoint = ObjectCamera.MainCamera.WorldToScreenPoint(transform.position);
                    Offset = transform.position - ObjectCamera.MainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z));
                    ObjectMovement.isFollowMouse = true;
                    ObjectMovement.isRotateObject = false;
                }
                else
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                {
                    ScreenPoint = ObjectCamera.SecondaryCamera.WorldToScreenPoint(transform.position);
                    Offset = transform.position - ObjectCamera.SecondaryCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z));
                    ObjectMovement.isFollowMouse = true;
                    ObjectMovement.isRotateObject = false;
                }
            }
            if (GlobalMouse.MouseType == GlobalMouse.CMouseType.MousePanZ)
            {
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                {
                    ScreenPoint = ObjectCamera.MainCamera.WorldToScreenPoint(transform.position);
                    Offset = transform.position - ObjectCamera.MainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z));
                    ObjectMovement.isFollowMouse = true;
                    ObjectMovement.isRotateObject = true;
                }
                else
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                {
                    ScreenPoint = ObjectCamera.SecondaryCamera.WorldToScreenPoint(transform.position);
                    Offset = transform.position - ObjectCamera.SecondaryCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z));
                    ObjectMovement.isFollowMouse = true;
                    ObjectMovement.isRotateObject = true;
                }
            }
        }

        void OnMouseDrag()
        {
            if (ObjectMovement.isFollowMouse && ObjectMovement.isRotateObject == true) {
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                {
                    Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z);
                    Vector3 curPosition = ObjectCamera.MainCamera.ScreenToWorldPoint(curScreenPoint) + Offset;
                    if (transform.position != curPosition) transform.LookAt(curPosition);
                    transform.position = curPosition;
                } else if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                {
                    Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z);
                    Vector3 curPosition = ObjectCamera.SecondaryCamera.ScreenToWorldPoint(curScreenPoint) + Offset;
                    if (transform.position != curPosition) transform.LookAt(curPosition);
                    transform.position = curPosition;
                }
            }
            if (ObjectMovement.isFollowMouse && ObjectMovement.isRotateObject == false)
            {
                if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraPrimary)
                {
                    Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z);
                    Vector3 curPosition = ObjectCamera.MainCamera.ScreenToWorldPoint(curScreenPoint) + Offset;
                    transform.position = curPosition;
                }
                else if (CameraSSD.CameraType == CameraSSD.CCameraType.IsCameraSecondary)
                {
                    Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, ScreenPoint.z);
                    Vector3 curPosition = ObjectCamera.SecondaryCamera.ScreenToWorldPoint(curScreenPoint) + Offset;
                    transform.position = curPosition;
                }
            }
        }

        void OnMouseUp()
        {
            ObjectMovement.isFollowMouse = false;
        }

        
        void OnTriggerEnter(Collider collider)
        {
            //-- bound position
            //detectPosition = collider.ClosestPoint(transform.position);

            if (collider.gameObject.tag == ObjectTerrain.TerrainTag || collider.gameObject.tag == ObjectTerrain.GroundTag ||
                collider.gameObject.tag == ObjectTerrain.Map2DTag || collider.gameObject.tag == ObjectTerrain.Map3DTag)
            {
                ObjectMovement.isGround = true;
                UnitConsole.PrintDebug("UnitManager (716): " + collider.gameObject.name + ": " + ObjectMovement.isGround.ToString());
            }
            if (collider.gameObject.tag == ObjectTerrain.OceanTag)
            {
                ObjectMovement.isOcean = true;
                UnitConsole.PrintDebug("UnitManager (721): " + collider.gameObject.name + ": " + ObjectMovement.isOcean.ToString());
            }

        }

        void OnTriggerExit(Collider collider)
        {
            //detectPosition = collider.ClosestPoint(transform.position);

            if (collider.gameObject.tag == ObjectTerrain.TerrainTag || collider.gameObject.tag == ObjectTerrain.GroundTag ||
                collider.gameObject.tag == ObjectTerrain.Map2DTag || collider.gameObject.tag == ObjectTerrain.Map3DTag)
            {
                //-- bound position
                //Vector3 boundPosition = detectPosition;
                //boundPosition.y = boundPosition.y + 5;
                //if (transform.position.y > boundPosition.y)
                //{
                //    ObjectMovement.isGround = false;
                //}

                ObjectMovement.isGround = false;
            }
            if (collider.gameObject.tag == ObjectTerrain.OceanTag)
            {
                ObjectMovement.isOcean = false;
            }
        }

        void DetectHumanGround()
        {
            if (ObjectHuman.isEnabled)
            {
                Vector3 boundPosition = detectPosition;
                boundPosition.y = boundPosition.y + 5;

                if (transform.position.y > boundPosition.y)
                {
                    ObjectMovement.isGround = false;
                }
            }
        }

        void SetAircraftSFX(bool aStatus)
        {
            if (ObjectAircraft.isEnabled)
            {
                for (int i = 0; i < ObjectAircraft.AircraftSettings.Length; i++)
                {
                   if (ObjectAircraft.AircraftSettings[i].usingSFXSelection)
                   {
                       for (int j = 0; j < ObjectAircraft.AircraftSettings[i].SFXSelection.Length; j++)
                       {
                           ObjectAircraft.AircraftSettings[i].SFXSelection[j].SetActive(aStatus);
                       }
                   }
                }
            }
        }

        void SetVehicleSFX(bool aStatus)
        {
            if (ObjectVehicle.isEnabled)
            {
                for (int i = 0; i < ObjectVehicle.VehicleSettings.Length; i++)
                {
                    if (ObjectVehicle.VehicleSettings[i].usingSFXSelection)
                    {
                        for (int j = 0; j < ObjectVehicle.VehicleSettings[i].SFXSelection.Length; j++)
                        {
                            ObjectVehicle.VehicleSettings[i].SFXSelection[j].SetActive(aStatus);
                        }
                    }
                }

            }
        }

        void SetTankTurret()
        {

            //-- TANK COLLISION SETTINGS 
            if (ObjectVehicle.isEnabled)
            {
                for (int i = 0; i < ObjectVehicle.VehicleSettings.Length; i++)
                {
                    if (ObjectVehicle.VehicleSettings[i].usingRotationHorizontal)
                    {
                        if (Input.GetKey(ObjectVehicle.VehicleSettings[i].RotateLeft))
                        {
                            ObjectVehicle.VehicleSettings[i].TurretObjectH.transform.Rotate(0, 0, -1);
                        }
                        if (Input.GetKey(ObjectVehicle.VehicleSettings[i].RotateRight))
                        {
                            ObjectVehicle.VehicleSettings[i].TurretObjectH.transform.Rotate(0, 0, 1);
                        }
                    }
                }
                for (int i = 0; i < ObjectVehicle.VehicleSettings.Length; i++)
                {
                    if (ObjectVehicle.VehicleSettings[i].usingRotationVertical)
                    {
                        if (Input.GetKey(ObjectVehicle.VehicleSettings[i].RotateUp))
                        {
                            ObjectVehicle.VehicleSettings[i].TurretObjectH.transform.Rotate(-1, 0, 0);
                        }
                        if (Input.GetKey(ObjectVehicle.VehicleSettings[i].RotateDown))
                        {
                            ObjectVehicle.VehicleSettings[i].TurretObjectH.transform.Rotate(1, 0, 0);
                        }
                    }
                }
            }

        }

        void SetAircraftRotor()
        {
            //-- HELICOPTER COLLISION SETTINGS 
            if (ObjectAircraft.isEnabled)
            {
                for (int i = 0; i < ObjectAircraft.AircraftSettings.Length; i++)
                {
                    if (ObjectAircraft.AircraftSettings[i].usingRotor)
                    {
                        for (int j = 0; j < ObjectAircraft.AircraftSettings[i].RotorObject.Length; j++)
                        {
                            if (ObjectMovement.isSelected)
                            {
                                ObjectAircraft.AircraftSettings[i].RotorObject[j].transform.Rotate(ObjectAircraft.AircraftSettings[i].RotateDirection[j] * rotorSpeed);
                                rotorSpeed += 0.01f;
                                if (rotorSpeed >= ObjectAircraft.AircraftSettings[i].RotorSpeed)
                                {
                                    rotorSpeed = ObjectAircraft.AircraftSettings[i].RotorSpeed;
                                }
                            }
                            else
                            {
                                ObjectAircraft.AircraftSettings[i].RotorObject[j].transform.Rotate(ObjectAircraft.AircraftSettings[i].RotateDirection[j] * rotorSpeed);
                                rotorSpeed -= 0.01f;
                                if (rotorSpeed <= 0)
                                {
                                    rotorSpeed = 0;
                                }
                            }
                        }
                    }
                }
            }
        }

        void SetAircraftLandingGear()
        {
            //-- AIRCRAFT COLLISION SETTINGS 
            if (ObjectAircraft.isEnabled)
            {
                for (int i = 0; i < ObjectAircraft.AircraftSettings.Length; i++)
                {
                    if (ObjectAircraft.AircraftSettings[i].usingLandingGear)
                    {
                        for (int j = 0; j < ObjectAircraft.AircraftSettings[i].LandingObject.Length; j++)
                        {
                            if (transform.position.y >= ObjectAircraft.AircraftSettings[i].LandingHeight)
                            {
                                ObjectAircraft.AircraftSettings[i].LandingObject[j].SetActive(false);
                            }
                            else
                            {
                                ObjectAircraft.AircraftSettings[i].LandingObject[j].SetActive(true);
                            }
                        }
                    }
                }
            }
        }

        void SetIdleArmyAnimation()
        {
            //-- MOVING SETTINGS
            for (int i = 0; i < ObjectHuman.MovingState3D.Length; i++)
            {
                for (int j = 0; j < ObjectHuman.MovingState3D[i].TriggerKey.Length; j++)
                {
                    if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Float)
                    {
                        float dummyvalue = float.Parse(ObjectHuman.MovingState3D[i].NegativeValue);
                        ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                        if (ObjectHuman.MovingState3D[i].ForceAnimation)
                        {
                            if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                            {
                                ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                            }
                        }
                    }
                }
            }
        }

        void SetArmyMovingAnimation()
        {
            //-- MOVING SETTINGS
            for (int i = 0; i < ObjectHuman.MovingState3D.Length; i++)
            {
                if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Float)
                {
                    float dummyvalue = float.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                    ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                    if (ObjectHuman.MovingState3D[i].ForceAnimation)
                    {
                        if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                        {
                            ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                        }
                    }
                }
                if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Int)
                {
                    int dummyvalue = int.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                    ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                    if (ObjectHuman.MovingState3D[i].ForceAnimation)
                    {
                        if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                        {
                            ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                            }
                        }
                    }
                if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Bool)
                {
                    bool dummyvalue = bool.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                    ObjectHuman.TargetAnimator.SetBool(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                    if (ObjectHuman.MovingState3D[i].ForceAnimation)
                    {
                        if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                        {
                            ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                        }
                    }
                }
                if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Trigger)
                {
                    ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.MovingState3D[i].ParameterName);
                    if (ObjectHuman.MovingState3D[i].ForceAnimation)
                    {
                        if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                        {
                            ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                        }
                    }
                }
            }
        }

        void SetArmyAnimation()
        {
            //-- MOVING SETTINGS
            if (ObjectMovement.isSelected && ObjectHuman.usingMovingState)
            {
                for (int i = 0; i < ObjectHuman.MovingState3D.Length; i++)
                {
                    for (int j = 0; j < ObjectHuman.MovingState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKey(ObjectHuman.MovingState3D[i].TriggerKey[j]))
                        {
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                                ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                    ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                            ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                    ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(ObjectHuman.MovingState3D[i].PositiveValue);
                                ObjectHuman.TargetAnimator.SetBool(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                    ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.MovingState3D[i].ParameterName);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                        }

                        if (Input.GetKeyUp(ObjectHuman.MovingState3D[i].TriggerKey[j]))
                        {
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ObjectHuman.MovingState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(ObjectHuman.MovingState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(ObjectHuman.MovingState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetBool(ObjectHuman.MovingState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.MovingState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.MovingState3D[i].ParameterName);
                                if (ObjectHuman.MovingState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.MovingState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.MovingState3D[i].StateNext);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //-- ACTION SETTINGS
            if (ObjectMovement.isSelected && ObjectHuman.usingActionState)
            {
                for (int i = 0; i < ObjectHuman.ActionState3D.Length; i++)
                {
                    for (int j = 0; j < ObjectHuman.ActionState3D[i].TriggerKey.Length; j++)
                    {
                        if (Input.GetKey(ObjectHuman.ActionState3D[i].TriggerKey[j]))
                        {
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ObjectHuman.ActionState3D[i].PositiveValue);
                                ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(ObjectHuman.ActionState3D[i].PositiveValue);
                                ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(ObjectHuman.ActionState3D[i].PositiveValue);
                                ObjectHuman.TargetAnimator.SetBool(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.ActionState3D[i].ParameterName);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                        }

                        if (Input.GetKeyUp(ObjectHuman.ActionState3D[i].TriggerKey[j]))
                        {
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Float)
                            {
                                float dummyvalue = float.Parse(ObjectHuman.ActionState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Int)
                            {
                                int dummyvalue = int.Parse(ObjectHuman.ActionState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Bool)
                            {
                                bool dummyvalue = bool.Parse(ObjectHuman.ActionState3D[i].NegativeValue);
                                ObjectHuman.TargetAnimator.SetBool(ObjectHuman.ActionState3D[i].ParameterName, dummyvalue);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                            if (ObjectHuman.ActionState3D[i].ParameterType == CParameterType.Trigger)
                            {
                                ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.ActionState3D[i].ParameterName);
                                if (ObjectHuman.ActionState3D[i].ForceAnimation)
                                {
                                    if (!ObjectHuman.TargetAnimator.GetCurrentAnimatorStateInfo(0).IsName(ObjectHuman.ActionState3D[i].StateNext))
                                    {
                                        ObjectHuman.TargetAnimator.Play(ObjectHuman.ActionState3D[i].StateNext);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            //-- SHUTDOWN SETTINGS
            if (ObjectMovement.isSelected && ObjectHuman.usingShutdownState)
            {
                for (int i = 0; i < ObjectHuman.ShutdownState3D.ShutdownTestkey.Length; i++)
                {
                    if (Input.GetKey(ObjectHuman.ShutdownState3D.ShutdownTestkey[i]))
                    {
                        Shutdown(true);
                    }
                }
            }

        }

        public void Shutdown(bool aValue)
        {
            if (ObjectHuman.usingShutdownState)
            {
                if (ObjectHuman.ShutdownState3D.ParameterType == CParameterType.Float)
                {
                    float dummyvalue = float.Parse(ObjectHuman.ShutdownState3D.PositiveValue);
                    ObjectHuman.TargetAnimator.SetFloat(ObjectHuman.ShutdownState3D.ParameterName, dummyvalue);
                    Destroy(this.gameObject, 5);
                }
                if (ObjectHuman.ShutdownState3D.ParameterType == CParameterType.Int)
                {
                    int dummyvalue = int.Parse(ObjectHuman.ShutdownState3D.PositiveValue);
                    ObjectHuman.TargetAnimator.SetInteger(ObjectHuman.ShutdownState3D.ParameterName, dummyvalue);
                    Destroy(this.gameObject, 5);
                }
                if (ObjectHuman.ShutdownState3D.ParameterType == CParameterType.Bool)
                {
                    bool dummyvalue = bool.Parse(ObjectHuman.ShutdownState3D.PositiveValue);
                    ObjectHuman.TargetAnimator.SetBool(ObjectHuman.ShutdownState3D.ParameterName, dummyvalue);
                    Destroy(this.gameObject, 5);
                }
                if (ObjectHuman.ShutdownState3D.ParameterType == CParameterType.Trigger)
                {
                    ObjectHuman.TargetAnimator.SetTrigger(ObjectHuman.ShutdownState3D.ParameterName);
                    Destroy(this.gameObject, 5);
                }
            }
        }

        float GetAxisHorizontal()
        {
            float result = 0;
            if (Input.GetKey(ObjectMovement.LeftKey) || Input.GetKey(ObjectMovement.AltLeftKey))
            {
                result = -1;
            }
            if (Input.GetKey(ObjectMovement.RightKey) || Input.GetKey(ObjectMovement.AltRightKey))
            {
                result = 1;
            }
            return result;
        }

        float GetAxisVertical()
        {
            float result = 0;
            if (Input.GetKey(ObjectMovement.UpKey) || Input.GetKey(ObjectMovement.AltUpKey))
            {
                result = 1;
            }
            if (Input.GetKey(ObjectMovement.DownKey) || Input.GetKey(ObjectMovement.AltDownKey))
            {
                result = -1;
            }
            return result;
        }

        void SyncronizeUnitPanel()
        {
            ObjectXML.MovementType = ConvertMovement();
            ObjectXML.Gravity = ObjectMovement.Gravity.ToString();
            ObjectXML.MoveSpeed = ObjectMovement.MoveSpeed.ToString();
            ObjectXML.RotateSpeed = ObjectMovement.RotateSpeed.ToString();
            ObjectXML.ScaleValue = ObjectMovement.ScaleValue.ToString();

            ObjectXML.TargetPositionX = ObjectTarget.TargetController.transform.position.x.ToString();
            ObjectXML.TargetPositionY = ObjectTarget.TargetController.transform.position.y.ToString();
            ObjectXML.TargetPositionZ = ObjectTarget.TargetController.transform.position.z.ToString();
            ObjectXML.TargetRotationX = ObjectTarget.TargetController.transform.localRotation.x.ToString();
            ObjectXML.TargetRotationY = ObjectTarget.TargetController.transform.localRotation.y.ToString();
            ObjectXML.TargetRotationZ = ObjectTarget.TargetController.transform.localRotation.z.ToString();
            ObjectXML.TargetScaleX = ObjectTarget.TargetController.transform.localScale.x.ToString();
            ObjectXML.TargetScaleY = ObjectTarget.TargetController.transform.localScale.y.ToString();
            ObjectXML.TargetScaleZ = ObjectTarget.TargetController.transform.localScale.z.ToString();

            ObjectXML.UID = ObjectTarget.TargetController.name;

            string[] objData = ObjectTarget.TargetController.name.Split('.');

            ObjectXML.Username = objData[0];
            ObjectXML.ObjectName = objData[3]+ objData[4]+ objData[5];
            ObjectXML.Postfix = ObjectTarget.TargetController.tag;

        }

        void SyncronizeUnitPanelOnScreen()
        {
            ObjectXML.UnitPanelObject = GameObject.Find(ObjectXML.UnitPanelName);
            if (ObjectXML.UnitPanelObject != null)
            {
                ObjectXML.UnitPanelObject.GetComponent<UnitClose>().OnSceeenUnitPanel();

                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtMovementType.text = ObjectXML.MovementType;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtGravity.text = ObjectXML.Gravity;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtMoveSpeed.text = ObjectXML.MoveSpeed;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtRotationSpeed.text = ObjectXML.RotateSpeed;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtScaleValue.text = ObjectXML.ScaleValue;

                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtPositionX.text = ObjectXML.TargetPositionX;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtPositionY.text = ObjectXML.TargetPositionY;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtPositionZ.text = ObjectXML.TargetPositionZ;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtRotationX.text = ObjectXML.TargetRotationX;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtRotationY.text = ObjectXML.TargetRotationY;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtRotationZ.text = ObjectXML.TargetRotationZ;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtScaleX.text = ObjectXML.TargetScaleX;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtScaleY.text = ObjectXML.TargetScaleY;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtScaleZ.text = ObjectXML.TargetScaleZ;

                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtUID.text = ObjectXML.UID;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtUsername.text = ObjectXML.Username;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtObjectname.text = ObjectXML.ObjectName;
                ObjectXML.UnitPanelObject.GetComponent<UnitBrowser>().txtPostfix.text = ObjectXML.Postfix;
            }
        }

        string ConvertMovement (){
            string result = "";

            switch (ObjectMovement.MovementType)
            {
                case CObjectMovement.CMovementType.TransformMode:
                    result = "TransformMode";
                break;

                case CObjectMovement.CMovementType.TranslateMode:
                    result = "TranslateMode";
                    break;
            }

            return result;
        }

        public void CloseUnitPanel()
        {
            //ObjectXML.UnitPanel.SetActive(false);
        }

        public void LoadObjectByName(string aName, GameObject aPosition)
        {
            AssetsDataObject.GetComponent<AssetBundleLoader>().InstantiateObjects(aName, aPosition.transform.position);
        }

        public void LoadObjectByName(string aName, GameObject aPosition, GameObject aRotation)
        {
            AssetsDataObject.GetComponent<AssetBundleLoader>().InstantiateObjects(aName, aPosition.transform.position, aRotation.transform.rotation);
        }


    }
}
